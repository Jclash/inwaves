#CC = mpiicc
#CC = c++
#CC = mpicc
#CC = x86_64-w64-mingw32-c++

sys=pc

ifeq '$(sys)' 'pc'
$(info system is pc)
else ifeq '$(sys)' '10p'
$(info system is MVS-10P)
else ifeq '$(sys)' 'gov'
$(info system is Govorun)
else
$(info system is unknown)
endif


##############
## COMPILER ##
##############

CC_PC=c++
CC_MSC=mpiicc
CC_GOV=c++
CC_DEF=$(CC_PC)

ifeq '$(sys)' 'pc'
CC = $(CC_PC)
else ifeq '$(sys)' '10p'
CC = $(CC_MSC)
else ifeq '$(sys)' 'gov'
CC = $(CC_GOV)
else
CC = $(CC_DEF)
endif


#############
## CPARAMS ##
#############

#CPARAMS_PC=-std=c++11 -s -lgsl -static-libgcc -Wl,-Bstatic -lstdc++ -lpthread

CPARAMS_PC=-lgsl -lgslcblas -lm -lpthread
CPARAMS_MSC=-lgsl -lgslcblas -lm
CPARAMS_GOV=-lgsl -lgslcblas -lm
CPARAMS_DEF=$(CPARAMS_PC)

ifeq '$(sys)' 'pc'
CPARAMS = $(CPARAMS_PC)
else ifeq '$(sys)' '10p'
CPARAMS = $(CPARAMS_MSC)
else ifeq '$(sys)' 'gov'
CPARAMS = $(CPARAMS_GOV)
else
CPARAMS = $(CPARAMS_DEF)
endif


############
## CFLAGS ##
############

MPIINCLUDE=-I/usr/include/mpich

#GSLINCLUDE=-I/usr/include/
GSLINCLUDE1=-I/nfs/hybrilit.jinr.ru/user/k/knyazkov/soft/include
GSLINCLUDE2=-I/usr/include
GSLINCLUDE3=-I/home4/pstorage1/ipmex4/include
GSLINCLUDE4=-I/home5/ipmex8/soft/gsl/include

# ARMADILLO
ARMAINCLUDE=-I/usr/include/armadillo_bits


#CFLAGS = $(MPIINCLUDE) -fopenmp
CFLAGS = -std=c++11 $(GSLINCLUDE1) $(GSLINCLUDE2) $(GSLINCLUDE3) $(GSLINCLUDE4)


############
## LFLAGS ##
############

# Hydra
GSLLIBS1 = -L/nfs/hybrilit.jinr.ru/user/k/knyazkov/soft/lib
# Lenovo Cygwin
GSLLIBS2 = -L/lib
# JSCC
GSLLIBS3 = -L/home4/pstorage1/ipmex4/lib
GSLLIBS4 = ''
# XeonXP Cygwin
GSLLIBS5 = -L/soft/gsl/lib
GSLLIBS6 = -L/home5/ipmex8/soft/gsl/lib


MPILIBS = -L/lib -L/usr/lib

MPI_FLAGS = -L/usr/lib -lmpi -lopen-rte -lopen-pal -lhwloc -levent_core -levent_pthreads

LAPACKLIBS = -L/soft/lapack++/lapackpp/lib


LFLAGS = $(GSLLIBS5) $(MPILIBS) $(GSLLIBS6)


LFLAGS_PC=-L/home/dmitri/lib
LFLAGS_MSC=-L/home5/ipmex8/soft/gsl/lib
LFLAGS_GOV=-L/nfs/hybrilit.jinr.ru/user/k/knyazkov/soft/lib
LFLAGS_DEF=$(LFLAGS_PC)

ifeq '$(sys)' 'pc'
LFLAGS = $(LFLAGS_PC)
else ifeq '$(sys)' '10p'
LFLAGS = $(LFLAGS_MSC)
else ifeq '$(sys)' 'gov'
LFLAGS = $(LFLAGS_GOV)
else
LFLAGS = $(LFLAGS_DEF)
endif


#LFLAGS += $(shell mpicc -showme:link)

#EXETARGET = tuvp2vr.exe

# Lenovo Laptop Cygwin
#PROJ_PATH = /home/User/work/dpproj

# Hydra
#PROJ_PATH = ~/work/dpproj

# Ubuntu
#PROJ_PATH = ~/work/dpproj

# LENOVO-PC
#PROJ_PATH = ~/work/dpproj_new/dpproj

# MSC
PROJ_PATH = .

SRC_PATH = $(PROJ_PATH)/src
BIN_PATH = $(PROJ_PATH)/bin
OUT_PATH = $(PROJ_PATH)/output
POSTPROC_PATH = $(PROJ_PATH)/postprocessing
SCRIPTS_PATH = $(SRC_PATH)/scripts

PLOTLAYERSCRIPT = plotlayer1.txt

PLOTCELLSCRIPT = plotsection1.inp



.PHONY: plots, clean, clobber


run: build
	./rs
	
runpp: buildpp
	./rs-inwaves-pp	

plots: run
	echo 'plots'
	./rs-pp


noplots: build
	./rs-noplots

#plots: run $(SCRIPTS_PATH)/$(PLOTCELLSCRIPT)
#	@echo
#	@echo Plotting results...
#	./ps


build: $(BIN_PATH)/run $(BIN_PATH)/run-pp $(SRC_PATH)/main.cpp $(SRC_PATH)/inwaves.cpp $(SRC_PATH)/inwaves.h $(SRC_PATH)/inwaves-pp.cpp $(SRC_PATH)/inwaves-pp.h

buildpp: $(BIN_PATH)/run-pp $(SRC_PATH)/inwaves-pp.cpp $(SRC_PATH)/inwaves-pp.h

$(BIN_PATH)/main.o : $(SRC_PATH)/main.cpp
	$(CC) $(CFLAGS) -c $(SRC_PATH)/main.cpp -o $(BIN_PATH)/main.o

$(BIN_PATH)/inwaves.o : $(SRC_PATH)/inwaves.cpp $(SRC_PATH)/inwaves.h
	$(CC) $(CFLAGS) -c $(SRC_PATH)/inwaves.cpp -o $(BIN_PATH)/inwaves.o

$(BIN_PATH)/run: $(BIN_PATH)/main.o $(BIN_PATH)/inwaves.o
	$(CC) $(LFLAGS) $(BIN_PATH)/main.o $(BIN_PATH)/inwaves.o -o $(BIN_PATH)/run $(CPARAMS)
	
$(BIN_PATH)/inwaves-pp.o : $(SRC_PATH)/inwaves-pp.cpp $(SRC_PATH)/inwaves-pp.h
	$(CC) $(CFLAGS) -c $(SRC_PATH)/inwaves-pp.cpp -o $(BIN_PATH)/inwaves-pp.o	
	
$(BIN_PATH)/run-pp: $(BIN_PATH)/inwaves-pp.o
	$(CC) $(LFLAGS) $(BIN_PATH)/inwaves-pp.o -o $(BIN_PATH)/run-pp $(CPARAMS)	


#
#  HYDRA:
#
#	$(CC) -std=c++11 $(LFLAGS) -s $(BIN_PATH)/main.o $(BIN_PATH)/inwaves.o -o $(BIN_PATH)/run -lgsl -static-libgcc -Wl,-Bstatic -lstdc++ -lpthread -lm

#	$(CC) $(LFLAGS) -lm $(BIN_PATH)/main.o $(BIN_PATH)/inwaves.o -o $(BIN_PATH)/run





runall: $(OUT_PATH)/executed runpp
	@echo

#	./rs
#	> $(OUT_PATH)/executed
#	$(BIN_PATH)/run


$(OUT_PATH)/executed: $(BIN_PATH)/run ./input.txt
	./rs
	> $(OUT_PATH)/executed




#	./rs
#	> $(OUT_PATH)/executed
#	$(BIN_PATH)/run

$(OUT_PATH)/executed: $(BIN_PATH)/run ./input.txt
	./rs
	> $(OUT_PATH)/executed
	


builddll: $(BIN_PATH)/dpproj.dll

##$(BIN_PATH)/dpprojmno.o : $(SRC_PATH)/dpprojmno.cpp $(SRC_PATH)/dpprojmno.h
##	$(CC) $(CFLAGS) -c -BUILDING_DPPROJ_DLL $(SRC_PATH)/dpprojmno.cpp -o $(BIN_PATH)/dpprojmno.o

##$(BIN_PATH)/dpproj.o : $(SRC_PATH)/dpproj.cpp $(SRC_PATH)/dpproj.h
##	$(CC) $(CFLAGS) -c -BUILDING_DPPROJ_DLL $(SRC_PATH)/dpproj.cpp -o $(BIN_PATH)/dpproj.o

$(BIN_PATH)/dpproj.dll: $(BIN_PATH)/dpproj.o $(BIN_PATH)/dpprojmno.o
	$(CC) $(LFLAGS) $(BIN_PATH)/dpproj.o $(BIN_PATH)/dpprojmno.o -shared -o $(BIN_PATH)/dpproj.dll  -lgsl -lgslcblas -lm


#/////


solution.txt: $(EXETARGET)
	@echo
	@echo Running code...
	rm -f step*.txt
	tuvp2vr.exe
	cat ha > solution.txt

plots.png: solution.txt plot_solution.py
	@echo
	@echo Plotting results...
	rm -f plot*.png
	rm -f centerline*.png
	python plot_solution.py
	cat ha > plots.png

#
# copy from animate target:
#
#python animate_solution.py
#avconv -i plot_r%d.png solution_r.avi
#avconv -i plot_r_mpi%d.png solution_r_mpi.avi
#avconv -i plocm_I1%d.png solution_I1.avi
#avconv -i plocm_sigma_e%d.png solution_sigma_e.avi
#avconv -f image2 -i plotcm_I1%d.png -vcodec h264 -crf 1 -r 24 out.mov
#avconv -i plotcm_I1%d.png -s solution_I1.avi
#avconv -i plotcm_sigma_e%d.png solution_sigma_e.avi


animate: solution_T0.avi solution_I1.avi solution_taup_rr_t.avi solution_sigma_e.avi solution_S.avi solution_T0_t.avi
	@echo
	@echo Animating results...
#	rm -f *.avi

solution_T0.avi: plots.png
	rm -f solution_T0.avi
	$(AVICONV) $(AVIOPT) plotcm_T0%d.png solution_T0.avi

solution_I1.avi: plots.png
	rm -f solution_I1.avi
	$(AVICONV) $(AVIOPT) plotcm_I1%d.png solution_I1.avi

solution_taup_rr_t.avi:	plots.png
	rm -f solution_taup_rr_t.avi
	$(AVICONV) $(AVIOPT) plotcm_taup_rr_t%d.png solution_taup_rr_t.avi

solution_sigma_e.avi: plots.png
	rm -f solution_sigma_e.avi
	$(AVICONV) $(AVIOPT) plotcm_Sigma_e%d.png solution_sigma_e.avi

solution_T0_t.avi: plots.png
	rm -f solution_T0_t.avi
	$(AVICONV) $(AVIOPT) plotcm_T0_t%d.png solution_T0_t.avi

solution_S.avi:	plots.png
	rm -f solution_S.avi
	$(AVICONV) $(AVIOPT) plotcm_S%d.png solution_S.avi

clean:
	rm -f $(BIN_PATH)/*.o $(BIN_PATH)/run $(BIN_PATH)/*.dll $(OUT_PATH)/run $(OUT_PATH)/run.exe $(OUT_PATH)/run-pp $(OUT_PATH)/run-pp.exe $(OUT_PATH)/plotscript

clobber: clean
	rm -f solution.txt *.png

resclean:
	rm -f $(OUT_PATH)/*.txt
	rm -f $(OUT_PATH)/executed
	rm -f $(OUT_PATH)/output.txt
	rm -f $(OUT_PATH)/*.png $(OUT_PATH)/B*.txt $(OUT_PATH)/C*.txt $(OUT_PATH)/fig*.txt $(OUT_PATH)/*.out $(OUT_PATH)/*.avi $(OUT_PATH)/solution.txt $(OUT_PATH)/plots $(OUT_PATH)/Tmax*.txt $(OUT_PATH)/input.txt $(OUT_PATH)/output.txt
	rm -f $(OUT_PATH)/run.*/*
	rm -f $(OUT_PATH)/run.*/.hosts
	rm -f $(OUT_PATH)/plotscript
	rm -f $(OUT_PATH)/*
	rmdir $(OUT_PATH)/run.*


ppclean:
	rm -f $(POSTPROC_PATH)/*.txt
	rm -f $(POSTPROC_PATH)/*.png

imgclean:
	rm -f $(OUT_PATH)/*.png

allclean: ppclean clean resclean
	rm *~ *dump ./set

cleanall: allclean
	
	