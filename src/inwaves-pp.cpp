#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <sstream>

#include <cmath>
#include <ctime>

using namespace std;

#include "inwaves-pp.h"


int preinit_params( params* pars, params_pp* pars_pp ){

  (*pars_pp).t1 = 0.0;
  (*pars_pp).t1_disc = "time start";

  (*pars_pp).t2 = 137.0;
  (*pars_pp).t2_disc = "time finish";

  (*pars_pp).x1 = -25.0;
  (*pars_pp).x1_disc = "x-coordinate of left bottom corner";

  (*pars_pp).y1 = -25.0;
  (*pars_pp).y1_disc = "y-coordinate of left bottom corner";

  (*pars_pp).x2 = 25.0;
  (*pars_pp).x2_disc = "x-coordinate of right upper corner";

  (*pars_pp).y2 = 25.0;
  (*pars_pp).y2_disc = "y-coordinate of right upper corner";

  (*pars_pp).q = (*pars).q; // step in time
  (*pars_pp).q_disc = "step in time";

  (*pars_pp).h = (*pars).h; // step along all axes
  (*pars_pp).h_disc = "step along all axes";

  (*pars_pp).Nt = ( (*pars_pp).t2 - (*pars_pp).t1 ) / (*pars_pp).h;  // points in time
  (*pars_pp).Nt_disc = "points in time";

  (*pars_pp).g = 9.81; // gravity acceleration
  (*pars_pp).g_disc = "gravity acceleration";

  (*pars_pp).smax = ceil(((*pars_pp).t2-(*pars_pp).t1)/(*pars_pp).q); // number of iterations
  (*pars_pp).smax_disc = "number of iterations";

  return 0;

};


int preinit_params( params* pars ){

  (*pars).meth = 2;
  (*pars).meth_disc = "problem formulation";

  (*pars).meth_fs_bc = 0;
  (*pars).meth_fs_bc_disc = "boundary conditions of free surface";

  (*pars).H = 25.0;  // half of the depth
  (*pars).H_disc = "half of the depth";

  (*pars).Nz = 63;  // points along z-axis
  (*pars).Nz_disc = "points along z-axis";

  (*pars).solver_tol = 0.000001;  /* solution relative tolerance */
  (*pars).solver_tol_disc = "solution relative tolerance";

  (*pars).solver_max_iter = 2; /* maximum iterations */
  (*pars).solver_max_iter_disc = "maximum iterations of solver";

  (*pars).rx = 4.0;  // width/height rate
  (*pars).rx_disc = "width/height rate";

  (*pars).ry = 1.0;  // length/height rate
  (*pars).ry_disc = "length/height rate";

  (*pars).a = 1.0; // x-coord of the velocity of the source0
  (*pars).a_disc = "x-coord of the velocity of the source";

  (*pars).b = 0.0; // y-coord of the velocity of the source
  (*pars).b_disc = "y-coord of the velocity of the source";

  (*pars).c = 0.0; // z-coord of the velocity of the source
  (*pars).c_disc = "z-coord of the velocity of the source";

  (*pars).N = 1.0; // buoyancy frequency
  (*pars).N_disc = "buoyancy frequency";

  (*pars).r = 0.5; // radius of the curve
  (*pars).r_disc = "radius of the curve N(z) near the jump";

  (*pars).rho0 = 1.0; // density of the liquid
  (*pars).rho0_disc = "density of the liquid";

  (*pars).hf = 20.0; // z=hf - plane of free surface
  (*pars).hf_disc = "plane of free surface";

  (*pars).p0 = 0.0; // atmospheric pressure
  (*pars).p0_disc = "atmospheric pressure";

  (*pars).g = 9.81; // gravity acceleration
  (*pars).g_disc = "gravity acceleration";

  (*pars).h = (2.0*(*pars).H)/(*pars).Nz; // step along all axes
  (*pars).h_disc = "step along all axes";

  (*pars).q = (*pars).h/10.0; // step in time
  (*pars).q_disc = "step in time";

  (*pars).Rx = 2.0*(*pars).H*(*pars).rx; // width
  (*pars).Rx_disc = "width";

  (*pars).Ry = 2.0*(*pars).H*(*pars).ry; // length
  (*pars).Ry_disc = "length";

  (*pars).source_start_x = - (*pars).Rx/4.0; //-(*pars).Rx/4.0; // - (*pars).Rx/2.0; //-(*pars).Rx/4.0; // start x-coordinate of the source
  (*pars).source_start_x_disc = "start x-coordinate of the source";

  (*pars).source_start_y = 0.0; // start y-coordinate of the source
  (*pars).source_start_y_disc = "start y-coordinate of the source";

  (*pars).source_start_z = 20.0;  // start z-coordinate of the source
  (*pars).source_start_z_disc = "start z-coordinate of the source";

  (*pars).Nx = (*pars).rx*(*pars).Nz; // points along x-axis
  (*pars).Nx_disc = "points along x-axis";

  (*pars).Ny = (*pars).ry*(*pars).Nz; // points along y-axis
  (*pars).Ny_disc = "points along y-axis";

  (*pars).xmin = -(*pars).Rx/2.0; // minimum of x coordinate
  (*pars).xmin_disc = "minimum of x coordinate";

  (*pars).xmax = (*pars).Rx/2.0; // maximum of x coordinate
  (*pars).xmax_disc = "maximum of x coordinate";

  (*pars).ymin = -(*pars).Ry/2.0; // minimum of y coordinate
  (*pars).ymin_disc = "minimum of y coordinate";

  (*pars).ymax = (*pars).Ry/2.0; // maximum of y coordinate
  (*pars).ymax_disc = "maximum of y coordinate";

  (*pars).zmin = -(*pars).H; // minimum of z coordinate
  (*pars).zmin_disc = "minimum of z coordinate";

  (*pars).zmax = (*pars).H; // maximum of z coordinate
  (*pars).zmax_disc = "maximum of z coordinate";

  (*pars).tmax = ((*pars).xmax-(*pars).source_start_x)/(*pars).a; // duration of movement
  (*pars).tmax_disc = "duration of movement";

  (*pars).p = 0.9; // ratio of mass of the source that lays inside the cutoff circle
  (*pars).p_disc = "ratio of mass of the source that lays inside the cutoff circle";

  (*pars).theta = 0.6; // threshold when displaying output fields as images
  (*pars).theta_disc = "threshold when displaying output fields as images";

  (*pars).R = 0.8845; // radius for cutoff
  (*pars).R_disc = "radius for cutoff";

  (*pars).Rprec = 0.001; // precision when finding radius for cutoff
  (*pars).Rprec_disc = "precision when finding radius for cutoff";

  (*pars).vect_space = (*pars).h; // space between vector start points
  (*pars).vect_space_disc = "space between vector start points";

  (*pars).vect_scale = 1.0; // vector output scale
  (*pars).vect_space_disc = "vector output scale";

  (*pars).vect_Rvect = 0.1*(*pars).h; // vector field cutoff
  (*pars).vect_Rvect_disc = "vector field cutoff";

  //int smax = 8; // number of iterations
  (*pars).smax = ceil((*pars).tmax/(*pars).q); // number of iterations
  (*pars).smax_disc = "number of iterations";

  (*pars).A = 1.0; // 6.0*1/sqrt(2.0); // parameter of the Gaussian function f
  (*pars).A_disc = "parameter of the Gaussian function f";

  (*pars).B = 1.0; // parameter of the Gaussian function f
  (*pars).B_disc = "parameter of the Gaussian function f";

  (*pars).Lon = 0.0; // parameter of the source function multiplyer function f_in, distance where f_in = 0
  (*pars).Lon_disc = "parameter of the source multiplier f_in";

  (*pars).Lin = 0.0; //(*pars).Rx/4.0; // parameter of the source function multiplyer function f_in, distance where f_in linearly increases
  (*pars).Lin_disc = "parameter of the source multiplier f_in";

  (*pars).sinLam = (*pars).Rx/2.0; // parameter of the sinusoidal path, wavelength
  (*pars).sinLam_disc = "parameter of the sinusoidal path, wavelength";

  (*pars).sinD = (*pars).sinLam/2.0/M_PI; // parameter of the sinusoidal path, amplitude
  (*pars).sinD_disc = "parameter of the sinusoidal path, amplitude";


  // MPI section

  (*pars).Na = 10;
  (*pars).sa = 1;
  (*pars).out_freq = 1;


  return 0;

};





bool i_am_verbose(int myrank){

    // return true;

  if ((myrank == 0)||(myrank == 0))
    return true;
  else
    return false;
}

double diffclock(double clock1, double clock2)
{
	//return ((clock2-clock1)*1000.0)/CLOCKS_PER_SEC;
	return ((clock2-clock1))/(double)CLOCKS_PER_SEC;
	//return clock2 - clock1;
}

double mclock(){
	return clock();
	//timeval tim;
	//gettimeofday(&tim, NULL);
	//return tim.tv_sec + (tim.tv_usec / 1000000.0);
}

int cur_pos(params pars, double* x0, double* y0, double* z0, int s){

  double t = pars.q*s;

  // TRANSLATIONAL MOTION
  *x0 = pars.source_start_x + pars.a*pars.q*s;
  *y0 = pars.source_start_y + pars.b*pars.q*s;
  *z0 = pars.source_start_z + pars.c*pars.q*s;

  // SINUSOIDAL MOTION
  //*x0 = pars.source_start_x + pars.a*t;
  //*y0 = pars.sinD*sin(2*M_PI*pars.a*t/pars.sinLam);
  //*z0 = 0.0;





  return 0;
}

double Nfunc(params pars, double z){


  return pars.N;

  //if( z >= 20.0 )
  //  return pars.N;
  //
  //if( z < 15.0 )
  //  return pars.N;

  double r = pars.r;
  double xc, yc;
  double Njump = 4*pars.N;

  if( z >= 20.0 )
    return pars.N;

  if(( 10.0 + r <= z )&&( z < 20.0 ))
    return Njump;

  if(( 10.0 < z )&&( z < 10.0 + r )){
    xc = 10.0 + r;
    yc = Njump - r;
    return yc + sqrt(r*r-(z-xc)*(z-xc));
  }

  if( z == 10.0 )
    return ( pars.N + Njump )/2.0;

  if(( 10.0 - r <= z )&&( z < 10.0 )){
    xc = 10.0 - r;
    yc = pars.N + r;
    return yc - sqrt(r*r-(z-xc)*(z-xc));
  }


  if( z < 10.0 - r )
    return pars.N;

  return pars.N;

};

double fin(params pars, double t){

  return 1.0;

  double res = 0.0;

  if( t >= ( pars.Lon + pars.Lin )/abs(pars.a) )
    return 1.0;

  if( t <= pars.Lon /abs(pars.a) )
    return 0.0;

  return (pars.a*t - pars.Lon)/pars.Lin;

};

int funcs0_triangulazation(params* pars, tfuncs* funcs){

  (*funcs).border = (int*)malloc(sizeof(int)*(*funcs).Nborder);

  double R = 1.0;

  (*funcs).Nnodes = (*funcs).Nborder;

  (*funcs).nodes = (double*)malloc(2*sizeof(double)*(*funcs).Nnodes);


  for( int i = 0; i < (*funcs).Nborder; i++ ){
    (*funcs).border[i] = i;
    (*funcs).nodes[i*2] = R*sin(2*M_PI/(*funcs).Nborder*i);
    (*funcs).nodes[i*2+1] = R*cos(2*M_PI/(*funcs).Nborder*i);
  }


  return 0;

}



int init_params_sp(params* pars, tfuncs* funcs){

  int myrank = 0, np = 1;

#if GO_MPI == 1
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

  (*pars).L = np - 1;
  (*pars).w = myrank;

  // double R0 = erfinv(p)/A;
  double R0 = (*pars).Rprec;

  double R = R0;
  while (erf((*pars).A*R)-2*(*pars).A/sqrt(M_PI)*R*exp(-(*pars).A*(*pars).A*R*R) < (*pars).p ){
    R = R + (*pars).Rprec;
  }
  (*pars).R = R;

  if ( TEXTOUT >= 3 ) {
    cout << "end of parameters init" << endl;
  }

  return 0;

}


int init_params_mpi(params* pars, tfuncs* funcs){

  int myrank = 0, np = 1;

#if GO_MPI == 1
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

  (*pars).L = np - 1;
  (*pars).w = myrank;

  double zmin = (*pars).zmin;
  double zmax = (*pars).zmax;

  (*pars).zmin = 2.0*(*pars).H/(*pars).L*( (*pars).w - 1) + zmin - (*pars).Na*(*pars).h; // minimum of y coordinate
  (*pars).zmax = 2.0*(*pars).H/(*pars).L*( (*pars).w ) + zmin + (*pars).Na*(*pars).h; // maximum of y coordinate

  if ( (*pars).w == 1 ){
    (*pars).zmin = zmin;
  }
  if ( (*pars).w == (*pars).L ){
    (*pars).zmax = zmax;
  }

  (*pars).Nz = ((*pars).zmax - (*pars).zmin)/(*pars).h;  // points along z-axis

  (*pars).H = ( (*pars).zmax - (*pars).zmin )/2.0;  // half of the depth

  (*pars).rx = (*pars).Rx/(2.0*(*pars).H);  // width/height rate
  (*pars).ry = (*pars).Ry/(2.0*(*pars).H);  // length/height rat

  (*pars).h = 2*(*pars).H/(*pars).Nz; // step along all axes
  //(*pars).q = (*pars).h/1.0; // step in time

  //(*pars).rx = ((*pars).xmax - (*pars).xmin)/((*pars).zmax - (*pars).zmin);  // width/height rate
  //(*pars).ry = ((*pars).ymax - (*pars).ymin)/((*pars).zmax - (*pars).zmin);  // length/height rate

  //(*pars).Rx = 2.0*(*pars).H*(*pars).rx; // width
  //(*pars).Ry = 2.0*(*pars).H*(*pars).ry; // length



  // double R0 = erfinv(p)/A;
  double R0 = (*pars).Rprec;

  double R = R0;
  while (erf((*pars).A*R)-2*(*pars).A/sqrt(M_PI)*R*exp(-(*pars).A*(*pars).A*R*R) < (*pars).p ){
    R = R + (*pars).Rprec;
  }
  (*pars).R = R;

  if ( TEXTOUT >= 3 ) {
    cout << "end of parameters init" << endl;
  }

  return 0;

}

double f( params pars, tfuncs funcs, int m, int n, int k, int s ){

  double A = pars.A;
  double B = pars.B;

  int Nx = pars.Nx;
  int Ny = pars.Ny;
  int Nz = pars.Nz;

  double x, y, z;
  double x0, y0, z0;

  //x0 = pars.source_start_x + pars.a*pars.q*(funcs.scur+1);
  //y0 = pars.source_start_y + pars.b*pars.q*(funcs.scur+1);
  //z0 = pars.source_start_z + pars.c*pars.q*(funcs.scur+1);

  // x = pars.xmin + pars.h*m;
  // y = pars.ymin + pars.h*n;
  // z = pars.zmin + pars.h*k;

  cur_pos(pars,&x0,&y0,&z0,s);

  x = funcs.xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
  y = funcs.ys[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
  z = funcs.zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

  double res = fin(pars,pars.q*s)*B*A*A*A*exp(-A*A*((x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0)))/sqrt(M_PI*M_PI*M_PI);

  //funcs.f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = fin(pars,pars.q*(funcs.scur+1))*B*A*A*A*exp(-A*A*((x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0)))/sqrt(M_PI*M_PI*M_PI);

  return res;
}

double dz_f( params pars, tfuncs funcs, int m, int n, int k, int s ){

  double h = pars.h;
  int Nz = pars.Nz;


  if( ( k > 0 ) && ( k < Nz )) {
    return ( f(pars,funcs,m,n,k+1,s) - f(pars,funcs,m,n,k-1,s) )/2.0/h;
  }

  if( k == 0 )
    return (2*f(pars,funcs,m,n,k+3,s) - 9*f(pars,funcs,m,n,k+2,s) + 18*f(pars,funcs,m,n,k+1,s) - 11*f(pars,funcs,m,n,k,s))/6.0/h;

  if( k == Nz )
    return (11*f(pars,funcs,m,n,k,s) - 18*f(pars,funcs,m,n,k-1,s) + 9*f(pars,funcs,m,n,k-2,s) - 2*f(pars,funcs,m,n,k-3,s))/6.0/h;

  return 0.0;
}


double dtdtdz_f( params pars, tfuncs funcs, int m, int n, int k, int s ){

  double A = pars.A;
  double B = pars.B;

  double a = pars.a;
  double b = pars.b;
  double c = pars.c;

  int Nx = pars.Nx;
  int Ny = pars.Ny;
  int Nz = pars.Nz;

  double x, y, z;
  double x0, y0, z0;

  //x0 = pars.source_start_x + pars.a*pars.q*(funcs.scur+1);
  //y0 = pars.source_start_y + pars.b*pars.q*(funcs.scur+1);
  //z0 = pars.source_start_z + pars.c*pars.q*(funcs.scur+1);

  // x = pars.xmin + pars.h*m;
  // y = pars.ymin + pars.h*n;
  // z = pars.zmin + pars.h*k;

  //cur_pos(pars,&x0,&y0,&z0,s);

  x0 = pars.source_start_x;
  y0 = pars.source_start_y;
  z0 = pars.source_start_z;

  double t = pars.q*s;

  x = funcs.xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
  y = funcs.ys[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
  z = funcs.zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

  double Expp = exp(-A*A*( (x-x0-a*t)*(x-x0-a*t) + (y-y0-b*t)*(y-y0-b*t) + (z-z0-c*t)*(z-z0-c*t) ));
  double D = a*(x-x0-a*t) + b*(y-y0-b*t) + c*(z-z0-c*t);

  return 4*B*A*A*A*A*A*A*A/sqrt(M_PI*M_PI*M_PI) * Expp * ( (z-z0-c*t)*(a*a+b*b+c*c-2*A*A*D*D) + 2*c*D );

  //
  // OLD NUMERICAL COMPUTATION
  //

  double q = pars.q;

  if( s > 0 ) {
    return (dz_f( pars, funcs, m, n, k, s + 1 ) - 2*dz_f( pars, funcs, m, n, k, s )  + dz_f( pars, funcs, m, n, k, s - 1 ))/q/q;
  }

  if( s == 0 )
    return ( -dz_f( pars, funcs, m, n, k, s + 3 ) + 4* dz_f( pars, funcs, m, n, k, s + 2 ) - 5*dz_f( pars, funcs, m, n, k, s + 1 ) + 2*dz_f( pars, funcs, m, n, k, s ) )/q/q;

  return 0.0;
}


int init_funcs_sp(params pars, tfuncs* funcs){

  int Nx = pars.Nx;
  int Ny = pars.Ny;
  int Nz = pars.Nz;

  double A = pars.A;
  double B = pars.B;

  double x, y, z;
  double x0, y0, z0;

  (*funcs).f = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); //  [Nx+1,Ny+1,Nz+1] values of the function that represents the source
  (*funcs).xs = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // [Nx+1,Ny+1,Nz+1] % x-coordinates of the computation grid
  (*funcs).ys = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // [Nx+1,Ny+1,Nz+1] % y-coordinates of the computation grid
  (*funcs).zs = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // [Nx+1,Ny+1,Nz+1] % z-coordinates of the computation grid

  (*funcs).p = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); //  [Nx+1,Ny+1,Nz+1] pressure
  (*funcs).zeta_sp = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)); //  [Nx+1,Ny+1] free surface around z=h_f

  (*funcs).psi = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)*4); // [Nx+1,Ny+1,Nz+1,4] - inner potential
  (*funcs).dpsi_dx = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)*4); // [Nx+1,Ny+1,Nz+1,4] - x-derivative of the inner potential
  (*funcs).dpsi_dy = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)*4); // [Nx+1,Ny+1,Nz+1,4] - y-derivative of the inner potential
  (*funcs).dpsi_dz = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)*4); // [Nx+1,Ny+1,Nz+1,4] - z-derivative of the inner potential
  (*funcs).zeta = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // [Nx+1,Ny+1,Nz+1] - vertical displacement
  (*funcs).hatzeta = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // [Nx+1,Ny+1,Nz+1] - normalized vertical displacement

  (*funcs).v_x = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // Nx+1,Ny+1,Nz+1] - x-component of the velocity
  (*funcs).v_y = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // Nx+1,Ny+1,Nz+1] - y-component of the velocity
  (*funcs).v_z = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // Nx+1,Ny+1,Nz+1] - z-component of the velocity

  (*funcs).v_x_mem = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)*4); // [Nx+1,Ny+1,Nz+1,4] - 4 last steps of x-component of the velocity
  (*funcs).v_y_mem = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)*4); // [Nx+1,Ny+1,Nz+1,4] - 4 last steps of x-component of the velocity

  (*funcs).dv_z_dx = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // Nx+1,Ny+1,Nz+1] - x-derivative of z-component of the velocity
  (*funcs).dv_z_dy = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // Nx+1,Ny+1,Nz+1] - y-derivative of z-component of the velocity

  (*funcs).v_x_an = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // Nx+1,Ny+1,Nz+1] - x-component of the velocity
  (*funcs).v_y_an = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // Nx+1,Ny+1,Nz+1] - y-component of the velocity
  (*funcs).v_z_an = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // Nx+1,Ny+1,Nz+1] - z-component of the velocity

  (*funcs).u = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)*4); // [Nx+1,Ny+1,Nz+1,4] -  velocity of fluid


  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){
      for( int k = 0; k < Nz+1; k++ ){

        x = pars.xmin + pars.h*m;
        y = pars.ymin + pars.h*n;
        z = pars.zmin + pars.h*k;

        x0 = pars.source_start_x;
        y0 = pars.source_start_y;
        z0 = pars.source_start_z;

        cur_pos(pars,&x0,&y0,&z0,0);

        (*funcs).xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = x;
        (*funcs).ys[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = y;
        (*funcs).zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = z;

        //(*funcs).f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = fin(pars,0.0)*B*A*A*A*exp(-A*A*((x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0)))/sqrt(M_PI*M_PI*M_PI);

        if ( pars.meth == 0 )
          (*funcs).f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = f(pars,(*funcs),m,n,k,0);
        if ( ( pars.meth == 1 ) || ( pars.meth == 2 ) )
          (*funcs).f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = dtdtdz_f(pars,(*funcs),m,n,k,0);

        //(*funcs).f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 2*B*A*A*A*A*A/sqrt(M_PI*M_PI*M_PI)*exp(2*A*A*((x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0)));

        (*funcs).zeta[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).hatzeta[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;

        (*funcs).p[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;

        (*funcs).u[ (Nx+1)*(Ny+1)*(Nz+1)*0 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).u[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;

        (*funcs).v_x_mem[ (Nx+1)*(Ny+1)*(Nz+1)*0 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).v_x_mem[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).v_x_mem[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).v_x_mem[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;

        (*funcs).v_y_mem[ (Nx+1)*(Ny+1)*(Nz+1)*0 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).v_y_mem[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).v_y_mem[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).v_y_mem[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;

        (*funcs).v_x[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).v_y[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).v_z[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;

        (*funcs).dv_z_dx[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).dv_z_dy[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;

      }

      (*funcs).zeta_sp[ m*(Ny+1) + n ] = 0.0;

    }
  }

  const size_t N = (Nx+1)*(Ny+1)*(Nz+1);

  if ( TEXTOUT >= 3 ) {

    cout << "N = " << N << endl;

  }

  (*funcs).Q = gsl_spmatrix_alloc_nzmax( N, N, (*funcs).PATTERN_SIZE*N, GSL_SPMATRIX_TRIPLET );  /* triplet format */
  (*funcs).Qc = gsl_spmatrix_alloc_nzmax( N, N, (*funcs).PATTERN_SIZE*N, GSL_SPMATRIX_CCS );  /* triplet format */
//  (*funcs).Q = gsl_spmatrix_alloc( N, N );  /* triplet format */
  (*funcs).b = gsl_vector_alloc( N );       /* right hand side vector */
  (*funcs).x = gsl_vector_alloc( N );       /* solution vector */


  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){
      for( int k = 0; k < Nz+1; k++ ){

//        x = pars.xmin + pars.h*m;
//        y = pars.ymin + pars.h*n;
//        z = pars.zmin + pars.h*k;
//
//        x0 = pars.source_start_x;
//        y0 = pars.source_start_y;
//        z0 = pars.source_start_z;
// cur_pos(pars,&x0,&y0,&z0,0);
//
//        (*funcs).f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = fin(pars,0.0)*B*A*A*A*exp(-A*A*((x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0)))/sqrt(M_PI*M_PI*M_PI);
//
//        (*funcs).xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = x;
//        (*funcs).ys[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = y;
//        (*funcs).zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = z;
//
//        (*funcs).zeta[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
//        (*funcs).hatzeta[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
//
//        (*funcs).u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
//        (*funcs).u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
//        (*funcs).u[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
//
//        //gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1) + n*(Nz+1) + k, m*(Ny+1)*(Nz+1) + n*(Nz+1) + k, 1.0);

        double coeff = 1.0/pars.h/pars.h/pars.q/pars.q;

        if( ( pars.meth == 0 ) || ( pars.meth == 1 ) ){
          if ((m == 0)||(n == 0)||(k == 0)||(m == Nx)||(n == Ny)||(k == Nz)){
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k ) , 1.0*coeff );
          }else{
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k ) , -6.0*coeff );
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, (m-1)*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k ) , 1.0*coeff );
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, (m+1)*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k ) , 1.0*coeff );
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + ( k ) , 1.0*coeff );
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + (n+1)*(Nz+1) + ( k ) , 1.0*coeff );
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + (k-1) , 1.0*coeff );
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + (k+1) , 1.0*coeff );
          }
        }

        if( pars.meth == 2 ){
          if ((m == 0)||(n == 0)||(k == 0)||(m == Nx)||(n == Ny)||(k == Nz)){
            if( ( k == Nz ) && ( m > 0 ) && ( n > 0 ) && ( m < Nx ) && ( n < Ny ) ){

              if( pars.meth_fs_bc == 0 ){
                gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k ) ,  1.5/pars.h/pars.q/pars.q );
                gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k-1 ) , -2.0/pars.h/pars.q/pars.q );
                gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k-2 ) , 0.5/pars.h/pars.q/pars.q );
              }

              if( pars.meth_fs_bc == 1 ){
                gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k ) ,  1.0/pars.h/pars.q/pars.q );
                gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k-1 ) , -1.0/pars.h/pars.q/pars.q );
              }

            }else{
              gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k ) , 1.0*coeff );
            }
          }else{
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k ) , -6.0*coeff );
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, (m-1)*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k ) , 1.0*coeff );
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, (m+1)*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k ) , 1.0*coeff );
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + ( k ) , 1.0*coeff );
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + (n+1)*(Nz+1) + ( k ) , 1.0*coeff );
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + (k-1) , 1.0*coeff );
            gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + (k+1) , 1.0*coeff );
          }
        }

      }
    }
  }

  gsl_spmatrix_scale( (*funcs).Q, 1.0 );

  //(*funcs).Qc = (gsl_spmatrix*) gsl_spmatrix_ccs( (*funcs).Q );


  if ( TEXTOUT >= 3 ) {

    cout << "Number of non-zero elements in Q: " << gsl_spmatrix_nnz( (*funcs).Q ) << endl;

    cout << "end of init" << endl;

  }

  return 0;

}

int init_funcs_mpi(params pars, tfuncs* funcs){

  int Nx = pars.Nx;
  int Ny = pars.Ny;
  int Nz = pars.Nz;

  double A = pars.A;
  double B = pars.B;

  double x, y, z;
  double x0, y0, z0;

  (*funcs).f = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); //  [Nx+1,Ny+1,Nz+1] values of the function that represents the source
  (*funcs).xs = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // [Nx+1,Ny+1,Nz+1] % x-coordinates of the computation grid
  (*funcs).ys = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // [Nx+1,Ny+1,Nz+1] % y-coordinates of the computation grid
  (*funcs).zs = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // [Nx+1,Ny+1,Nz+1] % z-coordinates of the computation grid

  (*funcs).p = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); //  [Nx+1,Ny+1,Nz+1] pressure
  (*funcs).zeta_sp = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)); //  [Nx+1,Ny+1] free surface around z=h_f

  (*funcs).psi = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)*4); // [Nx+1,Ny+1,Nz+1,4] - inner potential
  (*funcs).dpsi_dx = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)*4); // [Nx+1,Ny+1,Nz+1,4] - x-derivative of the inner potential
  (*funcs).dpsi_dy = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)*4); // [Nx+1,Ny+1,Nz+1,4] - y-derivative of the inner potential
  (*funcs).dpsi_dz = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)*4); // [Nx+1,Ny+1,Nz+1,4] - z-derivative of the inner potential
  (*funcs).zeta = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // [Nx+1,Ny+1,Nz+1] - vertical displacement
  (*funcs).hatzeta = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)); // [Nx+1,Ny+1,Nz+1] - normalized vertical displacement

  (*funcs).u = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(Nz+1)*4); // [Nx+1,Ny+1,Nz+1,4] -  velocity of fluid


  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){
      for( int k = 0; k < Nz+1; k++ ){

        x = pars.xmin + pars.h*m;
        y = pars.ymin + pars.h*n;
        z = pars.zmin + pars.h*k;

        x0 = pars.source_start_x;
        y0 = pars.source_start_y;
        z0 = pars.source_start_z;

        cur_pos(pars,&x0,&y0,&z0,0);

        (*funcs).f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = fin(pars,0.0)*B*A*A*A*exp(-A*A*((x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0)))/sqrt(M_PI*M_PI*M_PI);

        (*funcs).xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = x;
        (*funcs).ys[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = y;
        (*funcs).zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = z;

        (*funcs).zeta[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).hatzeta[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;

        (*funcs).p[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;

        (*funcs).u[ (Nx+1)*(Ny+1)*(Nz+1)*0 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
        (*funcs).u[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;

      }

      (*funcs).zeta_sp[ m*(Ny+1) + n ] = 0.0;

    }
  }

  const size_t N = (Nx+1)*(Ny+1)*(Nz+1);

  if ( TEXTOUT >= 3 ) {

    cout << "N = " << N << endl;

  }

  (*funcs).Q = gsl_spmatrix_alloc_nzmax( N, N, (*funcs).PATTERN_SIZE*N, GSL_SPMATRIX_TRIPLET );  /* triplet format */
  (*funcs).Qc = gsl_spmatrix_alloc_nzmax( N, N, (*funcs).PATTERN_SIZE*N, GSL_SPMATRIX_CCS );  /* triplet format */
//  (*funcs).Q = gsl_spmatrix_alloc( N, N );  /* triplet format */
  (*funcs).b = gsl_vector_alloc( N );       /* right hand side vector */
  (*funcs).x = gsl_vector_alloc( N );       /* solution vector */


  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){
      for( int k = 0; k < Nz+1; k++ ){

//        x = pars.xmin + pars.h*m;
//        y = pars.ymin + pars.h*n;
//        z = pars.zmin + pars.h*k;
//
//        x0 = pars.source_start_x;
//        y0 = pars.source_start_y;
//        z0 = pars.source_start_z;
// cur_pos(pars,&x0,&y0,&z0,0);
//
//        (*funcs).f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = fin(pars,0.0)*B*A*A*A*exp(-A*A*((x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0)))/sqrt(M_PI*M_PI*M_PI);
//
//        (*funcs).xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = x;
//        (*funcs).ys[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = y;
//        (*funcs).zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = z;
//
//        (*funcs).zeta[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
//        (*funcs).hatzeta[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
//
//        (*funcs).u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
//        (*funcs).u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
//        (*funcs).u[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
//
//        //gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1) + n*(Nz+1) + k, m*(Ny+1)*(Nz+1) + n*(Nz+1) + k, 1.0);

        if ((m == 0)||(n == 0)||(k == 0)||(m == Nx)||(n == Ny)||(k == Nz)){
          gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k ) , 1.0 );
        }else{
          gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k ) , -6.0 );
          gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, (m-1)*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k ) , 1.0 );
          gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, (m+1)*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + ( k ) , 1.0 );
          gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + ( k ) , 1.0 );
          gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + (n+1)*(Nz+1) + ( k ) , 1.0 );
          gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + (k-1) , 1.0 );
          gsl_spmatrix_set( (*funcs).Q, m*(Ny+1)*(Nz+1)+n*(Nz+1)+k, ( m )*(Ny+1)*(Nz+1) + ( n )*(Nz+1) + (k+1) , 1.0 );
        }

      }
    }
  }

  gsl_spmatrix_scale( (*funcs).Q, 1.0/pars.h/pars.h/pars.q/pars.q );

  //(*funcs).Qc = (gsl_spmatrix*) gsl_spmatrix_ccs( (*funcs).Q );


  if ( TEXTOUT >= 3 ) {

    cout << "Number of non-zero elements in Q: " << gsl_spmatrix_nnz( (*funcs).Q ) << endl;

    cout << "end of init" << endl;

  }

  return 0;

}


int init_params(  params* pars, tfuncs* funcs, params_pp* pars_pp  ){

  int myrank = 0, np = 1;

#if GO_MPI == 1
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

  preinit_params( pars );

  preinit_params( pars, pars_pp );

  if( ( np > 1 )&&( myrank > 0 ) )  {
    init_params_mpi( pars, funcs );
  }else{
    init_params_sp( pars, funcs );
  }

#if GO_MPI == 1
	MPI_Barrier( MPI_COMM_WORLD );
#endif

  return 0;

}

int init_funcs( params pars, tfuncs* funcs ){

  int myrank = 0, np = 1;

  (*funcs).scur = 0;
  (*funcs).PATTERN_SIZE = 7;

#if GO_MPI == 1
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

  if( ( np > 1 )&&( myrank > 0 ) )  {
    init_funcs_mpi( pars, funcs );
  }else{
    init_funcs_sp( pars, funcs );
  }

  int Nx = pars.Nx;
  int Ny = pars.Ny;
  int Nz = pars.Nz;

#if GO_MPI == 1
	MPI_Barrier( MPI_COMM_WORLD );

	MPI_Status* status;

  (*funcs).Nzs_at_proc0 = new int[pars.L];

  int* Nzs_tmp = new int[pars.L+1];


//  if( myrank > 0)
//    MPI_Send( &(pars.Nz), 1, MPI_INT, 0, myrank, MPI_COMM_WORLD);
//
//  if( myrank == 0){
//    for( int i = 0; i < pars.L; i++ ){
//      MPI_Recv( (*funcs).Nzs_at_proc0 + i, 1, MPI_INT, i+1, i+1, MPI_COMM_WORLD, status);
//    }
//  }

  MPI_Allgather( &(pars.Nz), 1, MPI_INT, Nzs_tmp, 1,  MPI_INT, MPI_COMM_WORLD );

  for( int i = 0; i < pars.L; i++)
    (*funcs).Nzs_at_proc0[i] = Nzs_tmp[i+1];

  MPI_Barrier( MPI_COMM_WORLD );

  if(( myrank == 0)&&( np > 1 )){
    if ( TEXTOUT >= 1 ) {

      cout << "Communication occured: " << "Nzs_at_proc0 = [ ";

      for( int i = 0; i < pars.L; i++ ){
        cout << ((*funcs).Nzs_at_proc0)[i] << " ";
      }

      cout << "]" << endl;

    }
  }

  if(( myrank == 0)&&( np > 1 )){

    int overallNz = 0;

    for( int i = 0; i < pars.L; i++ ){
      overallNz += (((*funcs).Nzs_at_proc0)[i]+1);
    }

    (*funcs).uws_at_proc0 = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(overallNz)*4);

  }

  if(( np > 1 )&&( myrank > 0)){

    if( myrank < pars.L ){
      (*funcs).Omega_w_plus_SEND = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(pars.Na+1)*4);
      (*funcs).Omega_w_plus_RECV = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(pars.Na+1)*4);
    }

    if( myrank > 1 ){
      (*funcs).Omega_w_minus_SEND = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(pars.Na+1)*4);
      (*funcs).Omega_w_minus_RECV = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1)*(pars.Na+1)*4);
    }

  }

#endif


  (*funcs).BC_top = new double[(Nx+1)*(Ny+1)]; // BC at the top
  (*funcs).BC_bottom = new double[(Nx+1)*(Ny+1)]; // BC at the bottom

  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){
      (*funcs).BC_top[ m*(Ny+1) + n ] = 0.0;
      (*funcs).BC_bottom[ m*(Ny+1) + n ] = 0.0;
    }
  }

  return 0;

}


int print_params(params pars, tfuncs funcs){

  cout << endl << "current parameters:" << endl;

  string delim = "  - ";

  cout << "meth = " << pars.meth << delim << pars.meth_disc << endl;
  cout << "meth_fs_bc = " << pars.meth_fs_bc << delim << pars.meth_fs_bc_disc << endl;
  cout << "H = " << pars.H << delim << pars.H_disc << endl;
  cout << "Nz = " << pars.Nz << delim << pars.Nz_disc << endl;
  cout << "rx = " << pars.rx << delim << pars.rx_disc << endl;
  cout << "ry = " << pars.ry << delim << pars.ry_disc << endl;
  cout << "a = " << pars.a << delim << pars.a_disc << endl;
  cout << "b = " << pars.b << delim << pars.b_disc << endl;
  cout << "c = " << pars.c << delim << pars.c_disc << endl;
  cout << "N = " << pars.N << delim << pars.N_disc << endl;
  cout << "r = " << pars.r << delim << pars.r_disc << endl;
  cout << "rho0 = " << pars.rho0 << delim << pars.rho0_disc << endl;
  cout << "p0 = " << pars.p0 << delim << pars.p0_disc << endl;
  cout << "hf = " << pars.hf << delim << pars.hf_disc << endl;
  cout << "g = " << pars.g << delim << pars.g_disc << endl;

  cout << "h = " << pars.h << delim << pars.h_disc << endl;
  cout << "q = " << pars.q << delim << pars.q_disc << endl;

  cout << "Rx = " << pars.Rx << delim << pars.Rx_disc << endl;
  cout << "Ry = " << pars.Ry << delim << pars.Ry_disc << endl;
  cout << "Nx = " << pars.Nx << delim << pars.Nx_disc << endl;
  cout << "Ny = " << pars.Ny << delim << pars.Ny_disc << endl;

  cout << "xmin = " << pars.xmin << delim << pars.xmin_disc << endl;
  cout << "xmax = " << pars.xmax << delim << pars.xmax_disc << endl;
  cout << "ymin = " << pars.ymin << delim << pars.ymin_disc << endl;
  cout << "ymax = " << pars.ymax << delim << pars.ymax_disc << endl;
  cout << "zmin = " << pars.zmin << delim << pars.zmin_disc << endl;
  cout << "zmax = " << pars.zmax << delim << pars.zmax_disc << endl;
  cout << "tmax = " << pars.tmax << delim << pars.tmax_disc << endl;

  cout << "p = " << pars.p << delim << pars.p_disc << endl;
  cout << "theta = " << pars.theta << delim << pars.theta_disc << endl;
  cout << "R = " << pars.R << delim << pars.R_disc << endl;
  cout << "Rprec = " << pars.Rprec << delim << pars.Rprec_disc << endl;
  cout << "vect_space = " << pars.vect_space << delim << pars.vect_space_disc << endl;
  cout << "vect_scale = " << pars.vect_scale << delim << pars.vect_scale_disc << endl;

  cout << "smax = " << pars.smax << delim << pars.smax_disc << endl;

  cout << "solver_tol = " << pars.solver_tol << delim << pars.solver_tol_disc << endl;
  cout << "solver_max_iter = " << pars.solver_max_iter << delim << pars.solver_max_iter_disc << endl;

  cout << "A = " << pars.A << delim << pars.A_disc << endl;
  cout << "B = " << pars.B << delim << pars.B_disc << endl;
  cout << "Lon = " << pars.Lon << delim << pars.Lon_disc << endl;
  cout << "Lin = " << pars.Lin << delim << pars.Lin_disc << endl;

  cout << "source_start_x = " << pars.source_start_x << delim << pars.source_start_x_disc << endl;
  cout << "source_start_y = " << pars.source_start_y << delim << pars.source_start_y_disc << endl;
  cout << "source_start_z = " << pars.source_start_z << delim << pars.source_start_z_disc << endl;

  cout << "sinLam = " << pars.sinLam << delim << pars.sinLam_disc << endl;
  cout << "sinD = " << pars.sinD << delim << pars.sinD_disc << endl;

  cout << endl;

  return 0;

}

int print_params_pp( params_pp pars_pp ){

  cout << endl << "current post processing parameters:" << endl;

  string delim = "  - ";

  cout << "t1 = " << pars_pp.t1 << delim << pars_pp.t1_disc << endl;
  cout << "t2 = " << pars_pp.t2 << delim << pars_pp.t2_disc << endl;
  cout << "h = " << pars_pp.h << delim << pars_pp.h_disc << endl;
  cout << "q = " << pars_pp.q << delim << pars_pp.q_disc << endl;
  cout << "Nt = " << pars_pp.Nt << delim << pars_pp.Nt_disc << endl;
  cout << "x1 = " << pars_pp.x1 << delim << pars_pp.x1_disc << endl;
  cout << "y1 = " << pars_pp.y1 << delim << pars_pp.y1_disc << endl;
  cout << "x2 = " << pars_pp.x2 << delim << pars_pp.x2_disc << endl;
  cout << "y2 = " << pars_pp.y2 << delim << pars_pp.y2_disc << endl;
  cout << "g = " << pars_pp.g << delim << pars_pp.g_disc << endl;
  cout << "smax = " << pars_pp.smax << delim << pars_pp.smax_disc << endl;

  cout << endl;

  return 0;

}

int process_funcs_prepare(params pars, tfuncs funcs){

  int Nx = pars.Nx;
  int Ny = pars.Ny;
  int Nz = pars.Nz;

  double A = pars.A;
  double B = pars.B;

  double x, y, z;
  double x0, y0, z0;

  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){
      for( int k = 0; k < Nz+1; k++ ){

        x0 = pars.source_start_x + pars.a*pars.q*funcs.scur;
        y0 = pars.source_start_y + pars.b*pars.q*funcs.scur;
        z0 = pars.source_start_z + pars.c*pars.q*funcs.scur;

        cur_pos(pars,&x0,&y0,&z0,funcs.scur);

        x = funcs.xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        y = funcs.ys[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        z = funcs.zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

        //funcs.f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = fin(pars,pars.q*funcs.scur)*B*A*A*A*exp(-A*A*((x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0)))/sqrt(M_PI*M_PI*M_PI);

        if ( pars.meth == 0 )
          funcs.f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = f(pars,funcs,m,n,k,funcs.scur);
        if ( ( pars.meth == 1 ) || ( pars.meth == 2 ) )
          funcs.f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = dtdtdz_f(pars,funcs,m,n,k,funcs.scur);

      }
    }
  }



  return 0;
}



int process_funcs_oneStep(params pars, tfuncs funcs){

   int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
//	cout << "my rank is" << myrank << endl;
#endif

  int Nx = pars.Nx;
  int Ny = pars.Ny;
  int Nz = pars.Nz;

  double A = pars.A;
  double B = pars.B;

  double x, y, z;
  double x0, y0, z0;

  double q = pars.q;
  double h = pars.h;

  //double N = pars.N;

  //
  //  MAIN SLE SOLVER GOES HERE
  //

  double RHS, Lxy_at_s, Lxyz_at_sm1, Lxyz_at_s;

  for( int m = 1; m <= Nx+1; m++ ){
    for( int n = 1; n <= Ny+1; n++ ){
      for( int k = 1; k <= Nz+1; k++ ){


        if((m == 1)||(n == 1)||(m == Nx+1)||(n == Ny+1)){

          RHS = 0.0;

        }

        if( k == 1 ){

          RHS = funcs.BC_bottom[ (m-1)*(Ny+1) + n-1 ];

        }

        if( (k == Nz+1) && ( (pars.meth == 0) || (pars.meth == 1) ) ){

          RHS = funcs.BC_top[ (m-1)*(Ny+1) + n-1 ];

        }

        if ( (k == Nz+1) && (pars.meth == 2) && ( m > 1 ) && ( n > 1 ) && ( m <= Nx ) && ( n <= Ny ) ){

          if( pars.meth_fs_bc == 0 ){

// 2u_z(s)/q^2
            RHS = 1/q/q/h*(    3*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1)]   \
                             - 4*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1-2)]   ) \
                             \
// -u_z(s-1)/q^2
               -0.5/q/q/h*(    3*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1)]   \
                             - 4*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1-2)]   )   \
                             \
// gu_xx(s)/h^2
               +pars.g/h/h*(     funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1+1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1)]   \
                             - 2*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1)]   )   \
                             \
// gu_yy(s)/h^2
               +pars.g/h/h*(     funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) + (n-1+1)*(Nz+1) + (k-1)]   \
                             - 2*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) + (n-1-1)*(Nz+1) + (k-1)]   );
          }

          if( pars.meth_fs_bc == 1 ){

// 2u_z(s)/q^2
            RHS = 2/q/q/h*(    1*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1)]   \
                             - 1*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1-1)] ) \
                             \
// -u_z(s-1)/q^2
                 -1/q/q/h*(    1*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1)]   \
                             - 1*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1-1)] )   \
                             \
// +gu_xx(s)/h^2
               +pars.g/h/h*(     funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1+1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1)]   \
                             - 2*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1)]   )   \
                             \
// +gu_yy(s)/h^2
               +pars.g/h/h*(     funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) + (n-1+1)*(Nz+1) + (k-1)]   \
                             - 2*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) + (n-1-1)*(Nz+1) + (k-1)]   );
          }

        }


        if( (m > 1) && (n > 1) && (k > 1) && (m < Nx+1) && (n < Ny+1) && (k < Nz+1) ) {

          z = funcs.zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

          //Lxy_at_s = 1/h^2*( u(m+1,n,k,2) + u(m-1,n,k,2) + u(m,n+1,k,2) + u(m,n-1,k,2) - 4*u(m,n,k,2) );

          Lxy_at_s = 1/h/h*(     funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1+1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) +   (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) +   (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) +   (n-1+1)*(Nz+1) + (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) +   (n-1-1)*(Nz+1) + (k-1)] \
                             - 4*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) +   (n-1)*(Nz+1) +   (k-1)]   );


          //Lxyz_at_sm1 = 1/h^2*( u(m+1,n,k,1) + u(m-1,n,k,1) + u(m,n+1,k,1) + u(m,n-1,k,1) + u(m,n,k+1,1) + u(m,n,k-1,1) - 6*u(m,n,k,1) );

          Lxyz_at_sm1 = 1/h/h*(  funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + (m-1+1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) +   (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + (m-1-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) +   (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + (m-1)*(Ny+1)*(Nz+1) +   (n-1+1)*(Nz+1) + (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + (m-1)*(Ny+1)*(Nz+1) +   (n-1-1)*(Nz+1) + (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + (m-1)*(Ny+1)*(Nz+1) +   (n-1)*(Nz+1) +   (k-1+1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + (m-1)*(Ny+1)*(Nz+1) +   (n-1)*(Nz+1) +   (k-1-1)] \
                             - 6*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + (m-1)*(Ny+1)*(Nz+1) +   (n-1)*(Nz+1) +   (k-1)]   );


          //Lxyz_at_s = 1/h^2*( u(m+1,n,k,2) + u(m-1,n,k,2) + u(m,n+1,k,2) + u(m,n-1,k,2) + u(m,n,k+1,2) + u(m,n,k-1,2) - 6*u(m,n,k,2) );

          Lxyz_at_s = 1/h/h*(    funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1+1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) +   (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) +   (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) +   (n-1+1)*(Nz+1) + (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) +   (n-1-1)*(Nz+1) + (k-1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) +   (n-1)*(Nz+1) +   (k-1+1)] \
                             +   funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) +   (n-1)*(Nz+1) +   (k-1-1)] \
                             - 6*funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) +   (n-1)*(Nz+1) +   (k-1)]   );


          RHS = -(Lxyz_at_sm1 - 2*Lxyz_at_s)/q/q - Nfunc(pars,z)*Nfunc(pars,z)*Lxy_at_s; // + funcs.f[ (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1) ];

        }

        RHS = RHS + dtdtdz_f(pars,funcs,m-1,n-1,k-1,funcs.scur);

        gsl_vector_set( funcs.b, (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1)+(k-1), RHS );

  //
  // copy u[2] to x (the first approximation for x)
  //

        gsl_vector_set( funcs.x, (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1)+(k-1), funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + (m-1)*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + (k-1)] );

      }
    }
  }

  /* now solve the system with the GMRES iterative solver */

    const size_t n = (Nx+1)*(Ny+1)*(Nz+1);
    const double tol = pars.solver_tol;  /* solution relative tolerance */
    const size_t max_iter = pars.solver_max_iter; /* maximum iterations */
    const gsl_splinalg_itersolve_type *T = gsl_splinalg_itersolve_gmres;
    gsl_splinalg_itersolve *work = gsl_splinalg_itersolve_alloc(T, n, 0);
    size_t iter = 0;
    double residual;
    int status;

    /* initial guess u = 0 */
    //gsl_vector_set_zero( funcs.x );

    /* solve the system A u = f */
    do
      {
        status = gsl_splinalg_itersolve_iterate( funcs.Q, funcs.b, tol, funcs.x, work);

        /* print out residual norm ||A*u - f|| */
        residual = gsl_splinalg_itersolve_normr(work);
        if (i_am_verbose(myrank))
          fprintf(stdout, "process %d: iter %zu residual = %.12e\n", myrank,iter, residual);

        if (status == GSL_SUCCESS){
          if (i_am_verbose(myrank))
            fprintf(stdout, "process %d: Converged\n", myrank);
        }
      }
    while (status == GSL_CONTINUE && ++iter < max_iter);

    gsl_splinalg_itersolve_free(work);


  //
  // copy x to u[3]
  //

  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){
      for( int k = 0; k < Nz+1; k++ ){

        funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] =  gsl_vector_get( funcs.x, m*(Ny+1)*(Nz+1) + n*(Nz+1) + k );

      }
    }
  }

  //
  // prepare f for the next step (s + 1)
  //

  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){
      for( int k = 0; k < Nz+1; k++ ){

        x0 = pars.source_start_x + pars.a*pars.q*(funcs.scur+1);
        y0 = pars.source_start_y + pars.b*pars.q*(funcs.scur+1);
        z0 = pars.source_start_z + pars.c*pars.q*(funcs.scur+1);

        cur_pos(pars,&x0,&y0,&z0,funcs.scur+1);

        x = funcs.xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        y = funcs.ys[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        z = funcs.zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

        //funcs.f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = fin(pars,pars.q*(funcs.scur+1))*B*A*A*A*exp(-A*A*((x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0)))/sqrt(M_PI*M_PI*M_PI);

        if ( pars.meth == 0 )
          funcs.f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = f(pars,funcs,m,n,k,funcs.scur+1);

        if ( ( pars.meth == 1 ) || ( pars.meth == 2 ) )
          funcs.f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = dtdtdz_f(pars,funcs,m,n,k,funcs.scur+1);


      }
    }
  }

  return 0;
}


int load_funcs(params pars, tfuncs funcs){

   int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
//	cout << "my rank is" << myrank << endl;
#endif

  int Nx = pars.Nx;
  int Ny = pars.Ny;
  int Nz = pars.Nz;

  double A = pars.A;
  double B = pars.B;

  double x0, y0, z0;

  double q = pars.q;
  double h = pars.h;

  //double N = pars.N;



  //
  // load v_z(x,y,H) to u[3]
  //

  int s = funcs.scur;

  ostringstream sstr;
  sstr << s;
  string si_str = sstr.str();

  string s_str = si_str;

  if (s<1000){
    s_str = "" + si_str;
  }
  if (s<100){
    s_str = "0" + si_str;
  }
  if (s<10){
    s_str = "00" + si_str;
  }

//  if (s-1 < 0) s++;
//  ostringstream sstr_mOne;
//  sstr_mOne << s-1;
//  string si_str_mOne = sstr_mOne.str();
//
//  string s_str_mOne = si_str_mOne;
//
//  if (s-1<100){
//    s_str_mOne = "0" + si_str_mOne;
//  }
//  if (s-1<10){
//    s_str_mOne = "00" + si_str_mOne;
//  }

    ifstream myfile;

    string filename = "sectionXY_vz_zEQH_at_" + s_str + ".txt";

 	myfile.open( filename );

//    for( int m = 0; m < Nx; m++ ){
//      for( int n = 0; n < Ny; n++ ){
//
//        myfile1 << xs[ m*(Ny) + n ] << " " << ys[ m*(Ny) + n ] << " " << f[ m*(Ny) + n ] << endl;
//      }
//
//      myfile1 << endl;
//    }


  double x, y, vz;

  if (!myfile.is_open())
        std::cout << "failed to open " << filename << '\n';
  else{
    for( int m = 0; m < Nx+1; m++ ){
      for( int n = 0; n < Ny+1; n++ ){

        myfile >> x;
        myfile >> y;
        myfile >> vz;

        //cout << "loaded: x = " << x << ", y = " << y << ", v_z = " << vz << endl;

        funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + Nz ] = vz;

      }
    }

    myfile.close();

  }




  return 0;
}



int u_shift(params pars, tfuncs funcs){

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
//	cout << "my rank is" << myrank << endl;
#endif

  int Nx = pars.Nx;
  int Ny = pars.Ny;
  int Nz = pars.Nz;

  if ( TEXTOUT >= 2 ) {
    if (i_am_verbose(myrank))
      cout << "process "<< myrank <<": u shift" << endl;
  }

  //
  // shift u[3] -> u[2] -> u[1] -> del
  //

  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){
      for( int k = 0; k < Nz+1; k++ ){

        funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*0 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

        funcs.v_x_mem[ (Nx+1)*(Ny+1)*(Nz+1)*0 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = funcs.v_x_mem[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        funcs.v_x_mem[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = funcs.v_x_mem[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        funcs.v_x_mem[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = funcs.v_x_mem[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

        funcs.v_y_mem[ (Nx+1)*(Ny+1)*(Nz+1)*0 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = funcs.v_y_mem[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        funcs.v_y_mem[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = funcs.v_y_mem[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        funcs.v_y_mem[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = funcs.v_y_mem[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

      }
    }
  }

  return 0;

}


int obtain_current_fields( params pars, tfuncs funcs ){

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
//	cout << "my rank is" << myrank << endl;
#endif

  int Nx = pars.Nx;
  int Ny = pars.Ny;
  int Nz = pars.Nz;

  double h = pars.h;
  double q = pars.q;

  if ( TEXTOUT >= 2 ) {
    if (i_am_verbose(myrank))
      cout << "process " << myrank << ": obtaining current fields" << endl;
  }

  //
  // PROBLEM FOR \PSI
  //

  if ( pars.meth == 0 ){

  // [Nx+1,Ny+1,Nz+1,3] - inner potential \psi
  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){
      for( int k = 0; k < Nz+1; k++ ){
        for( int p = 0; p < 4; p++ ){

          funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*p + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*p + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

        }
      }
    }
  }

  // [Nx+1,Ny+1,Nz+1,3] - x-derivative of the inner potential
  // [Nx+1,Ny+1,Nz+1,3] - y-derivative of the inner potential
  // [Nx+1,Ny+1,Nz+1,3] - z-derivative of the inner potential
  for( int m = 0; m < Nx; m++ ){
    for( int n = 0; n < Ny; n++ ){
      for( int k = 0; k < Nz; k++ ){
        for( int p = 0; p < 4; p++ ){

          funcs.dpsi_dx[ (Nx+1)*(Ny+1)*(Nz+1)*p + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = (funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*p + (m+1)*(Ny+1)*(Nz+1) + (n+0)*(Nz+1) + (k+0) ] - funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*p + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ])/h;
          funcs.dpsi_dy[ (Nx+1)*(Ny+1)*(Nz+1)*p + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = (funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*p + (m+0)*(Ny+1)*(Nz+1) + (n+1)*(Nz+1) + (k+0) ] - funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*p + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ])/h;
          funcs.dpsi_dz[ (Nx+1)*(Ny+1)*(Nz+1)*p + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = (funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*p + (m+0)*(Ny+1)*(Nz+1) + (n+0)*(Nz+1) + (k+1) ] - funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*p + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ])/h;

          if ( m == Nx-1 )
            funcs.dpsi_dx[ (Nx+1)*(Ny+1)*(Nz+1)*p + (Nx)*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = (funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*p + (Nx)*(Ny+1)*(Nz+1) + (n+0)*(Nz+1) + (k+0) ] - funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*p + (Nx-1)*(Ny+1)*(Nz+1) + n*(Nz+1) +      k ])/h;

          if ( n == Ny-1 )
            funcs.dpsi_dy[ (Nx+1)*(Ny+1)*(Nz+1)*p + (m)*(Ny+1)*(Nz+1) + Ny*(Nz+1) + k ] = (funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*p + (m)*(Ny+1)*(Nz+1) + (Ny)*(Nz+1) +   (k+0) ] - funcs.psi[  (Nx+1)*(Ny+1)*(Nz+1)*p + m*(Ny+1)*(Nz+1) +    (Ny-1)*(Nz+1) +  k ])/h;

          if ( k == Nz-1 )
            funcs.dpsi_dz[ (Nx+1)*(Ny+1)*(Nz+1)*p + (m)*(Ny+1)*(Nz+1) + n*(Nz+1) + Nz ] = (funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*p + (Nx)*(Ny+1)*(Nz+1) + (n+0)*(Nz+1) + (Nz)  ] - funcs.psi[  (Nx+1)*(Ny+1)*(Nz+1)*p + m*(Ny+1)*(Nz+1) +     n*(Nz+1) +    (Nz-1) ])/h;

        }
      }
    }
  }

  // pressure
  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){
      for( int k = 0; k < Nz+1; k++ ){

          funcs.p[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] =
          -pars.rho0*( - 1*funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*0 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ]                    \
                       + 3*funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ]                    \
                       - 3*funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ]                    \
                       + 1*funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] )/h/h/h            \
          -pars.rho0*pars.N*pars.N*(   funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ]           \
                                     - funcs.psi[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] )/h;

      }
    }
  }

  // free surface zeta_sp
  int k_f = pars.hf/pars.h + Nz/2;
  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){

          funcs.zeta_sp[ m*(Ny+1) + n ] = pars.hf + ( funcs.p[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k_f ] - pars.p0 )/pars.g/pars.rho0;

    }
  }


  // [Nx+1,Ny+1,Nz+1] - vertical displacement
  for( int m = 0; m < Nx; m++ ){
    for( int n = 0; n < Ny; n++ ){
      for( int k = 0; k < Nz; k++ ){

        funcs.zeta[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = (funcs.dpsi_dz[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] - funcs.dpsi_dz[(Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ])/q;

      }
    }
  }

  double z;
  int Nvar;

  // [Nx+1,Ny+1,Nz+1] - normalized vertical displacement
  for( int m = 0; m < Nx; m++ ){
    for( int n = 0; n < Ny; n++ ){
      for( int k = 0; k < Nz; k++ ){

        z = funcs.zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        Nvar = Nfunc(pars,z);

        funcs.hatzeta[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 2*M_PI*pars.a*pars.a*pars.a/Nvar/Nvar/pars.B*funcs.zeta[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

      }
    }
  }

  // [Nx+1,Ny+1,Nz+1] - v_z
  for( int m = 0; m < Nx; m++ ){
    for( int n = 0; n < Ny; n++ ){
      for( int k = 0; k < Nz; k++ ){

        z = funcs.zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        Nvar = Nfunc(pars,z);

        funcs.v_x[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = (       \
                                                          funcs.dpsi_dx[(Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] \
                                                       -2*funcs.dpsi_dx[(Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] \
                                                        + funcs.dpsi_dx[(Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] \
                                                       )/q/q  \
                                                       + Nvar*Nvar*funcs.dpsi_dx[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        funcs.v_y[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = (funcs.dpsi_dy[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] - 2*funcs.dpsi_dy[(Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] + funcs.dpsi_dy[(Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ])/q/q \
                                                       + Nvar*Nvar*funcs.dpsi_dy[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        funcs.v_z[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = (funcs.dpsi_dz[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] - 2*funcs.dpsi_dz[(Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] + funcs.dpsi_dz[(Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ])/q/q;

      }
    }
  }

  }

  //
  // PROBLEM FOR V_Z
  //

  if ( ( pars.meth == 1 ) || ( pars.meth == 2 ) ){

  // [Nx+1,Ny+1,Nz+1] - v_z
  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){
      for( int k = 0; k < Nz+1; k++ ){

        funcs.v_z[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

      }
    }
  }

  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){

      int kmax =  Nz;

      // dv_z_dx
      if( m == 0 )
        funcs.dv_z_dx[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] = (1./2.0/pars.h)*( - 1*funcs.v_z[ (m+2)*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ]  \
                                                                               + 4*funcs.v_z[ (m+1)*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ]  \
                                                                               - 3*funcs.v_z[ (m+0)*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] );
      if( m == Nx )
        funcs.dv_z_dx[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] = (1./2.0/pars.h)*(   3*funcs.v_z[ (m+0)*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ]  \
                                                                               - 4*funcs.v_z[ (m-1)*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ]  \
                                                                               + 1*funcs.v_z[ (m-2)*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] );

      if( ( m > 0) && ( m < Nx ) )
        funcs.dv_z_dx[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] = (1./2.0/pars.h)*(   1*funcs.v_z[ (m+1)*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] \
                                                                               - 1*funcs.v_z[ (m-1)*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ]);

      // dv_z_dy
      if( n == 0 )
        funcs.dv_z_dy[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] = (1./2.0/pars.h)*( - 1*funcs.v_z[ m*(Ny+1)*(Nz+1) + (n+2)*(Nz+1) + kmax ]  \
                                                                               + 4*funcs.v_z[ m*(Ny+1)*(Nz+1) + (n+1)*(Nz+1) + kmax ]  \
                                                                               - 3*funcs.v_z[ m*(Ny+1)*(Nz+1) + (n+0)*(Nz+1) + kmax ] );
      if( n == Ny )
        funcs.dv_z_dy[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] = (1./2.0/pars.h)*(   3*funcs.v_z[ m*(Ny+1)*(Nz+1) + (n+0)*(Nz+1) + kmax ]  \
                                                                               - 4*funcs.v_z[ m*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + kmax ]  \
                                                                               + 1*funcs.v_z[ m*(Ny+1)*(Nz+1) + (n-2)*(Nz+1) + kmax ] );

      if( ( n > 0) && ( n < Ny ) )
        funcs.dv_z_dy[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] = (1./2.0/pars.h)*(   1*funcs.v_z[ m*(Ny+1)*(Nz+1) + (n+1)*(Nz+1) + kmax ] \
                                                                               - 1*funcs.v_z[ m*(Ny+1)*(Nz+1) + (n-1)*(Nz+1) + kmax ]);

    }
  }

  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){

      int kmax =  Nz;

      funcs.v_x_mem[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] =      2*funcs.v_x_mem[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] \
                                                                                       - 1*funcs.v_x_mem[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ]  \
                                                                                       - pars.g*pars.q*pars.q*funcs.dv_z_dx[  m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ];

      funcs.v_y_mem[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] =      2*funcs.v_y_mem[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] \
                                                                                       - 1*funcs.v_y_mem[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ]  \
                                                                                       - pars.g*pars.q*pars.q*funcs.dv_z_dy[  m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ];

      funcs.v_x[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] = funcs.v_x_mem[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ];

      funcs.v_y[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ] = funcs.v_y_mem[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + kmax ];

    }
  }


  }


  return 0;

}



int obtain_analytic_fields( params pars, tfuncs funcs ){

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
//	cout << "my rank is" << myrank << endl;
#endif

  int Nx = pars.Nx;
  int Ny = pars.Ny;
  int Nz = pars.Nz;

  double h = pars.h;
  double q = pars.q;

  if ( TEXTOUT >= 2 ) {
    if (i_am_verbose(myrank))
      cout << "process " << myrank << ": obtaining analytic fields" << endl;
  }

  //
  // prepare f for the next step (s + 1)
  //

  double x0, y0, z0;
  double x, y, z;
  double r1x, r1y, r1z;
  double theta1, phi1, r1;
  double Rx, Ry, Rz, R;

  for( int m = 0; m < Nx+1; m++ ){
    for( int n = 0; n < Ny+1; n++ ){
      for( int k = 0; k < Nz+1; k++ ){

        x0 = pars.source_start_x + pars.a*pars.q*(funcs.scur+1);
        y0 = pars.source_start_y + pars.b*pars.q*(funcs.scur+1);
        z0 = pars.source_start_z + pars.c*pars.q*(funcs.scur+1);

        cur_pos(pars,&x0,&y0,&z0,funcs.scur+1);

        x = funcs.xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        y = funcs.ys[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
        z = funcs.zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

        r1x = x - x0;
        r1y = y - y0;
        r1z = z - z0;

        r1 = sqrt(r1x*r1x + r1y*r1y + r1z*r1z );
        theta1 = acos(r1x/r1);
        phi1 = atan((z-z0)/(y-y0));

        Rx = -r1*tan(theta1)*sin(theta1);
        Ry =  r1*tan(theta1)*cos(theta1)*cos(phi1);
        Rz =  r1*tan(theta1)*cos(theta1)*sin(phi1);
        R = sqrt( Rx*Rx + Ry*Ry + Rz*Rz );

        //funcs.f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = B*A*A*A*exp(-A*A*((x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0)))/sqrt(M_PI*M_PI*M_PI);
        double Hx1 = 0.0;
        if ( x - x0 > 0 )
          Hx1 = 1.0;

        double m0 = 1.0;
        double U = pars.a;
        funcs.v_z_an[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = Hx1*pars.N*m0/2.0/M_PI/U/r1*(sin(theta1)*sin(theta1)+cos(theta1)*cos(theta1)*cos(phi1)*cos(phi1))/sin(theta1)*Rz/R*cos(pars.N/U*r1*abs(sin(phi1)));

      }
    }
  }



  return 0;

}



int collect_psi_on_proc0( params pars, tfuncs funcs ){

    int myrank = 0, np = 1;

    int Nx = pars.Nx;
    int Ny = pars.Ny;
    int Nz = pars.Nz;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
//	cout << "my rank is" << myrank << endl;

    if((myrank == 1)||(myrank == 3)){
      for( int m = 0; m < Nx+1; m++ ){
       for( int n = 0; n < Ny+1; n++ ){
         for( int k = 0; k < Nz+1; k++ ){

            //funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*0 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = myrank;
            //funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = myrank;
            //funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = myrank;
            //funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = myrank;

          }
        }
      }
    }


    int* displs = (int *)malloc(np*sizeof(int));
    int* rcounts = (int *)malloc(np*sizeof(int));

    displs[0] = 0;
    rcounts[0] = 0;
    int cur_disp = 0;
    for ( int i = 0; i < pars.L; i++ ) {
      displs[i+1] = cur_disp;
      rcounts[i+1] = (funcs.Nzs_at_proc0[i]+1)*(Nx + 1)*(Ny + 1)*4;
      cur_disp += rcounts[i+1];
    }

    MPI_Gatherv( funcs.u, rcounts[myrank], MPI_DOUBLE, funcs.uws_at_proc0, rcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    MPI_Barrier( MPI_COMM_WORLD );

  if(( myrank == 0)&&( np > 1 )){
    if ( TEXTOUT >= 1 ) {

      cout << "Communication occured: " << "MPI_Gatherv at collect_psi_on_proc0" << endl;

    }
  }

  if(( myrank == 0)&&( np > 1 )){



      for( int m = 0; m < Nx+1; m++ ){
       for( int n = 0; n < Ny+1; n++ ){
         for( int k = 0; k < Nz+1; k++ ){

            funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*0 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
            funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
            funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;
            funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = 0.0;

          }
        }
      }

      //int k_stack = 0;
      //int k_w = 0;
      //int k_real = 0;

      int k_shift = 0;

      int Nz_w = 0;

      //int Nz_processed = 0;

    for( int w_cur = 0; w_cur < pars.L; w_cur++ ){

      Nz_w = funcs.Nzs_at_proc0[w_cur];

      int Nz_w0 = funcs.Nzs_at_proc0[w_cur] - pars.Na*2;
      if (( w_cur == 0)||(w_cur == pars.L - 1))
        Nz_w0 = funcs.Nzs_at_proc0[w_cur] - pars.Na;

cout << "w_cur = " << w_cur << ", Nz_w = " << Nz_w << ", Nz_w0 = " << Nz_w0 << endl;

      for( int m = 0; m < Nx+1; m++ ){
       for( int n = 0; n < Ny+1; n++ ){
         for( int k = 0; k < Nz_w0+1; k++ ){

           int last_shift = pars.Na;
           if (w_cur == 0)
             last_shift = 0;

             if(w_cur > -1){
            funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*0 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k + k_shift] = funcs.uws_at_proc0[ displs[w_cur+1] + (Nx+1)*(Ny+1)*(Nz_w+1)*0 + m*(Ny+1)*(Nz_w+1) + n*(Nz_w+1) + k + last_shift];
            funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*1 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k + k_shift] = funcs.uws_at_proc0[ displs[w_cur+1] + (Nx+1)*(Ny+1)*(Nz_w+1)*1 + m*(Ny+1)*(Nz_w+1) + n*(Nz_w+1) + k + last_shift];;
            funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*2 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k + k_shift] = funcs.uws_at_proc0[ displs[w_cur+1] + (Nx+1)*(Ny+1)*(Nz_w+1)*2 + m*(Ny+1)*(Nz_w+1) + n*(Nz_w+1) + k + last_shift];;
            funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*3 + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k + k_shift] = funcs.uws_at_proc0[ displs[w_cur+1] + (Nx+1)*(Ny+1)*(Nz_w+1)*3 + m*(Ny+1)*(Nz_w+1) + n*(Nz_w+1) + k + last_shift];;
             }

          }
        }
      }

      k_shift += Nz_w0;

    }

  }

#endif

  return 0;

}


int exchange_Omegas( params pars, tfuncs funcs ){

int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);

    //MPI_Comm MPI_COMM_ALL_WS;

    //if( myrank > 0){
    //  MPI_Comm_create(MPI_COMM_WORLD, 1, &MPI_COMM_ALL_WS);
    //}

//    MPI_Comm_split(MPI_COMM_WORLD, int color, int key, MPI_Comm *newcomm);


    //if(myrank == 0)
    //  return 0;

    int Nx = pars.Nx;
    int Ny = pars.Ny;
    int Nz = pars.Nz;
    int Na = pars.Na;


    if( np > 1 ){

    for( int m = 0; m < Nx+1; m++ ){
     for( int n = 0; n < Ny+1; n++ ){
       for( int k = 0; k < Nz+1; k++ ){
         for( int j = 0; j < 4; j++ ){

            //funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*j + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = myrank*100 + k;

          }
        }
      }
    }

    }

    int i = 0;

    if(myrank > 0){

    // FILLING SEND BUFFERS
    if( myrank < np-1 ){

    for( int m = 0; m < Nx+1; m++ ){
     for( int n = 0; n < Ny+1; n++ ){
       for( int k = Nz-Na; k < Nz+1; k++ ){
         for( int j = 0; j < 4; j++ ){

            funcs.Omega_w_plus_SEND[i] = funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*j + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k - Na ];

            i++;

          }
        }
      }
    }

    }

    if( myrank > 1 ){

    i = 0;
    for( int m = 0; m < Nx+1; m++ ){
     for( int n = 0; n < Ny+1; n++ ){
       for( int k = 0; k < Na+1; k++ ){
         for( int j = 0; j < 4; j++ ){

            funcs.Omega_w_minus_SEND[i] = funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*j + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k + Na ];

            i++;
          }
        }
      }
    }

    }

    }

    MPI_Request request1;
    MPI_Request request2;
    MPI_Request request3;
    MPI_Request request4;

    double test = 8;
    double test_r = 9;

    MPI_Barrier( MPI_COMM_WORLD );

    if(myrank>0){

    // PASSING UP
    if (myrank < np - 1){
      MPI_Isend( funcs.Omega_w_plus_SEND, (Nx+1)*(Ny+1)*(pars.Na+1)*4, MPI_DOUBLE, myrank+1, funcs.scur, MPI_COMM_WORLD, &request1 );
    }

    if (myrank > 1){
      MPI_Irecv( funcs.Omega_w_minus_RECV, (Nx+1)*(Ny+1)*(pars.Na+1)*4, MPI_DOUBLE, myrank-1, funcs.scur, MPI_COMM_WORLD, &request2 );
    }

    }

    MPI_Barrier( MPI_COMM_WORLD );

    if(myrank>0){

    // PASSING DOWN
    if (myrank > 1){
      MPI_Isend( funcs.Omega_w_minus_SEND, (Nx+1)*(Ny+1)*(pars.Na+1)*4, MPI_DOUBLE, myrank-1, 1000+funcs.scur, MPI_COMM_WORLD, &request3 );
    }

    if (myrank < np - 1){
      MPI_Irecv( funcs.Omega_w_plus_RECV, (Nx+1)*(Ny+1)*(pars.Na+1)*4, MPI_DOUBLE, myrank+1, 1000+funcs.scur, MPI_COMM_WORLD, &request4);
    }

    }

    MPI_Barrier( MPI_COMM_WORLD );


//  if(( i_am_verbose(myrank))&&( np > 1 )){
//    if ( TEXTOUT >= 3 ) {
//
//      cout << "Communication occured: " << "MPI_Sends and MPI_Recvs at exchange_Omegas" << endl;
//
//    }
//  }

if(myrank > 0){

   // ECTRACTING FROM RECV BUFFERS
    if( myrank < np-1 ){

    i = 0;
    for( int m = 0; m < Nx+1; m++ ){
     for( int n = 0; n < Ny+1; n++ ){
       for( int k = Nz-Na; k < Nz+1; k++ ){
         for( int j = 0; j < 4; j++ ){

            //funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*j + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k  ] = funcs.Omega_w_plus_RECV[i];

            if(( k == Nz )&&( j == 3 )){

              funcs.BC_top[ m*(Ny+1) + n ] = funcs.Omega_w_plus_RECV[i];

            }

            i++;

          }
        }
      }
    }

    }

    if( myrank > 1 ){

    i = 0;
    for( int m = 0; m < Nx+1; m++ ){
     for( int n = 0; n < Ny+1; n++ ){
       for( int k = 0; k < Na+1; k++ ){
         for( int j = 0; j < 4; j++ ){

            //funcs.Omega_w_minus_SEND[i] = funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*j + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
            //funcs.u[ (Nx+1)*(Ny+1)*(Nz+1)*j + m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] = funcs.Omega_w_minus_RECV[i];

            if(( k == 0 )&&( j == 3 )){

              funcs.BC_bottom[ m*(Ny+1) + n ] = funcs.Omega_w_minus_RECV[i];;

            }

            i++;
          }
        }
      }
    }

    }

}

#endif

  return 0;

}

int process_funcs( params pars, tfuncs funcs, params_pp pars_pp, tfuncs_pp funcs_pp ){

   int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
//	cout << "my rank is" << myrank << endl;
#endif

  funcs.scur = 0;
  process_funcs_prepare( pars, funcs );

  funcs_pp.scur = 0;
  funcs.scur = (int) pars_pp.t1/pars_pp.q;

  if ( myrank == 0 ){
    obtain_current_fields( pars, funcs );
    //output_all_data( pars, funcs );
  }

  //
  // main calculation cycle
  //
  for( int s = 0; s < pars_pp.smax; s++ ){

    if ( TEXTOUT >= 2 ) {
      if (i_am_verbose(myrank))
        cout << "process "<< myrank <<": calculating at step " << s << endl;
    }

    // proc[0] does not perform computations in MPI mode
    if ( ( np == 1 ) || (( np > 1 )&&( myrank != 0 )) ){

      //process_funcs_oneStep( pars, funcs );
      load_funcs( pars, funcs );

    }

    funcs.scur += 1;
    funcs_pp.scur += 1;

    if(( (s+1) % pars.sa == 0 )&&( s > 0 )){

#if GO_MPI == 1

      MPI_Barrier( MPI_COMM_WORLD );
      exchange_Omegas( pars, funcs );
      MPI_Barrier( MPI_COMM_WORLD );

#endif

    }

    if( (((s+1) % pars.out_freq == 0 )&&( s > 0 )) || ( s == pars.smax - 1 )){

#if GO_MPI == 1

      MPI_Barrier( MPI_COMM_WORLD );
      collect_psi_on_proc0( pars, funcs );
      MPI_Barrier( MPI_COMM_WORLD );

#endif

      if ( myrank >= 0 ){
        obtain_current_fields( pars, funcs );
        //obtain_analytic_fields( pars, funcs );
        output_all_data_pp( pars, funcs );
      }

    }


    u_shift( pars, funcs );

  }

  return 0;

}


int output_all_data( params pars, tfuncs funcs ){

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
//	cout << "my rank is" << myrank << endl;
#endif

  int Nx = pars.Nx;
  int Ny = pars.Ny;
  int Nz = pars.Nz;

  int s = funcs.scur;

  if ( TEXTOUT >= 2 ) {
    if (i_am_verbose(myrank))
      cout << "process " << myrank << ": data output" << endl;
  }

  ostringstream sstr;
  sstr << s;
  string si_str = sstr.str();

  string s_str = si_str;

  if (s<100){
    s_str = "0" + si_str;
  }
  if (s<10){
    s_str = "00" + si_str;
  }

  ostringstream procsstr;
  procsstr << myrank;
  string proc_str = procsstr.str();

//  if (s-1 < 0) s++;
//  ostringstream sstr_mOne;
//  sstr_mOne << s-1;
//  string si_str_mOne = sstr_mOne.str();
//
//  string s_str_mOne = si_str_mOne;
//
//  if (s-1<100){
//    s_str_mOne = "0" + si_str_mOne;
//  }
//  if (s-1<10){
//    s_str_mOne = "00" + si_str_mOne;
//  }

  if(s == 0){
    output_sectionXZ_N_txt( pars, funcs.xs, funcs.zs, "sectionXZ_N_yEQ0_at_" + s_str );
  }

  //
  // OUTPUT FOR THE PROBLEM ON \PSI
  //
  if ( pars.meth == 0 ){

  // sections of psi
  //output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.psi + (Nx+1)*(Ny+1)*(Nz+1)*2,  0.0, "sectionXY_psi_zEQ0_at_" + s_str, 1 );
  //output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.psi + (Nx+1)*(Ny+1)*(Nz+1)*2,  pars.a, "sectionXY_psi_zEQa_at_" + s_str, 1 );
  //output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.psi + (Nx+1)*(Ny+1)*(Nz+1)*2,  pars.a/2.0, "sectionXY_psi_zEQha_at_" + s_str, 1 );
  //output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.psi + (Nx+1)*(Ny+1)*(Nz+1)*2,  0.0, "proc" + proc_str + "_" + "sectionXZ_psi_yEQ0_at_" + s_str, 0 );
  //output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.psi + (Nx+1)*(Ny+1)*(Nz+1)*2,  pars.a, "proc" + proc_str + "_" + "sectionXZ_psi_yEQa_at_" + s_str, 0 );


  // sections of \zeta
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.zeta, 0.0, "sectionXY_zeta_zEQ0_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.zeta, pars.a, "sectionXY_zeta_zEQa_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.zeta, pars.hf, "sectionXY_zeta_zEQhf_at_" + s_str, 1 );

/*
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.zeta, 25.0, "sectionXY_zeta_zEQ25_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.zeta, 30.0, "sectionXY_zeta_zEQ30_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.zeta, 20.0, "sectionXY_zeta_zEQ20_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.zeta, 15.0, "sectionXY_zeta_zEQ15_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.zeta, 10.0, "sectionXY_zeta_zEQ10_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.zeta, 5.0, "sectionXY_zeta_zEQ05_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.zeta, 4.0, "sectionXY_zeta_zEQ04_at_" + s_str, 1 );
*/

  output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.zeta, 0.0, "sectionXZ_zeta_yEQ0_at_" + s_str, 1 );
  output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.zeta, pars.a, "sectionXZ_zeta_yEQa_at_" + s_str, 1 );


  // sections of \hatzeta
  //output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.hatzeta, 0.0, "proc" + proc_str + "_" + "sectionXY_hatzeta_zEQ0_at_" + s_str, 1 );
  //output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.hatzeta, pars.a, "proc" + proc_str + "_" + "sectionXY_hatzeta_zEQa_at_" + s_str, 1 );
  //output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.hatzeta, pars.a/2.0, "sectionXY_hatzeta_zEQha_at_" + s_str, 1 );

  //output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.hatzeta, 0.0, "proc" + proc_str + "_" + "sectionXZ_hatzeta_yEQ0_at_" + s_str, 1 );
  //output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.hatzeta, pars.a/2.0, "sectionXZ_hatzeta_yEQha_at_" + s_str, 1 );
  //output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.hatzeta, pars.a, "proc" + proc_str + "_" + "sectionXZ_hatzeta_yEQa_at_" + s_str, 1 );

  // sections of v_z
  output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_z, 0.0, "sectionXZ_vz_yEQ0_at_" + s_str, 1 );
  output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_z, pars.a/2.0, "sectionXZ_vz_yEQha_at_" + s_str, 1 );
  output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_z, pars.a, "sectionXZ_vz_yEQa_at_" + s_str, 1 );

  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_z, 0.0, "sectionXY_vz_zEQ0_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_z, pars.a, "sectionXY_vz_zEQa_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_z, pars.hf, "sectionXY_vz_zEQhf_at_" + s_str, 1 );



  int nstep = (int)( pars.vect_space/pars.h );

  // vector fields of v

  output_vfieldXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_x, funcs.v_y, pars.hf, "vfieldXY_v_zEQhf_at_" + s_str, 0, 1, 1.0);

  //output_vfieldXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_x, funcs.v_z, 0.0, "vfieldXZ_v_yEQ0_at_" + s_str, 2, nstep);

/*
  output_vfieldXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_x, funcs.v_y, pars.hf, "vfieldXY_v_zEQhf_at_" + s_str, 0, 1, 1.0);

  output_vfieldXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_x, funcs.v_y, 4.0, "vfieldXY_v_zEQ04_at_" + s_str, 0, 1, 1.0);

  output_vfieldXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_x, funcs.v_y, 5.0, "vfieldXY_v_zEQ05_at_" + s_str, 0, 1, 1.0);

  output_vfieldXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_x, funcs.v_y, 10.0, "vfieldXY_v_zEQ10_at_" + s_str, 0, 1, 1.0);

  output_vfieldXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_x, funcs.v_y, 15.0, "vfieldXY_v_zEQ15_at_" + s_str, 0, 1, 1.0);

  output_vfieldXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_x, funcs.v_y, 20.0, "vfieldXY_v_zEQ20_at_" + s_str, 0, 1, 1.0);

  output_vfieldXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_x, funcs.v_y, 25.0, "vfieldXY_v_zEQ25_at_" + s_str, 0, 1, 1.0);

  output_vfieldXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_x, funcs.v_y, 30.0, "vfieldXY_v_zEQ30_at_" + s_str, 0, 1, 1.0);
*/


  //output_vfieldXZ_at_y_to_txt_cutARoundSource( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_x, funcs.v_z, 0.0, "vfieldXZ_cut_v_yEQ0_at_" + s_str, 2, 10.0, 10.0, 0.0);

  for( int m = 0; m < Nx; m++ ){
    for( int n = 0; n < Ny; n++ ){
      for( int k = 0; k < Nz; k++ ){

        funcs.v_x[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] -= pars.a;
        funcs.v_y[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] -= pars.b;
        funcs.v_z[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ] -= pars.c;

      }
    }
  }

// vector fields of v (in the moving frame)
  //output_vfieldXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_x, funcs.v_z, 0.0, "vfieldXZ_moving_v_yEQ0_at_" + s_str, 2, nstep);

  //output_vfieldXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_x, funcs.v_y, pars.hf, "vfieldXY_v_moving_zEQhf_at_" + s_str, 0, 1, 1.0);

  //output_vfieldXZ_at_y_to_txt_cutARoundSource( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_x, funcs.v_z, 0.0, "vfieldXZ_moving_cut_v_yEQ0_at_" + s_str, 2, 15, 15, -5.0);


////////////////////////////////////////
/// ANALYTICAL SOLUTION (VOISIN 1994:  J. Fluid Mech. (1994), V 261, pp. 333-374)
////////////////////////////////////////
/*
  // sections of v_z
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_z, 0.0, "proc" + proc_str + "_" + "sectionXY_vz_zEQ0_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_z, pars.a, "proc" + proc_str + "_" + "sectionXY_vz_zEQa_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_z, pars.a/2.0, "proc" + proc_str + "_" + "sectionXY_vz_zEQha_at_" + s_str, 1 );

  output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_z, 0.0, "proc" + proc_str + "_" + "sectionXZ_vz_yEQ0_at_" + s_str, 1 );
  output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_z, pars.a, "proc" + proc_str + "_" + "sectionXZ_vz_yEQa_at_" + s_str, 1 );
  output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_z, pars.a/2.0, "proc" + proc_str + "_" + "sectionXZ_vz_yEQha_at_" + s_str, 1 );


  // sections of v_z_an
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_z_an, 0.0, "proc" + proc_str + "_" + "sectionXY_vz_an_zEQ0_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_z_an, pars.a, "proc" + proc_str + "_" + "sectionXY_vz_an_zEQa_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_z_an, pars.a/2.0, "proc" + proc_str + "_" + "sectionXY_vz_an_zEQha_at_" + s_str, 1 );

  output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_z_an, 0.0, "proc" + proc_str + "_" + "sectionXZ_vz_an_yEQ0_at_" + s_str, 1 );
  output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_z_an, pars.a, "proc" + proc_str + "_" + "sectionXZ_vz_an_yEQa_at_" + s_str, 1 );
  output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_z_an, pars.a/2.0, "proc" + proc_str + "_" + "sectionXZ_vz_an_yEQha_at_" + s_str, 1 );
*/

  // sections of pressure
  //output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.p, pars.a, "proc" + proc_str + "_" + "sectionXY_p_zEQa_at_" + s_str, 1 );
  //output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.p, pars.a, "proc" + proc_str + "_" + "sectionXZ_p_yEQa_at_" + s_str, 1 );

  // free surface
  if( s >= 2 ){

    ostringstream s_minusOneAndHalf_str;
    s_minusOneAndHalf_str << (s-2);

    string si_str1 = s_minusOneAndHalf_str.str();

    string s_str1 = si_str1;

    if (s-2<100){
      s_str1 = "0" + si_str1;
    }
    if (s-2<10){
      s_str1 = "00" + si_str;
    }

    s_str1 = si_str1 + ".5";

    //output_2dFunctionOfXY_to_txt( pars, funcs.scur - 1.5, funcs.xs, funcs.ys, funcs.zeta_sp, "proc" + proc_str + "_" + "freeSurface_around_hf_at_" + s_str1, 0, pars.hf );
    //output_2dFunctionOfXY_to_txt( pars, funcs.scur - 1.5, funcs.xs, funcs.ys, funcs.zeta_sp, "proc" + proc_str + "_" + "freeSurface_noR_around_hf_at_" + s_str1, 1, pars.hf );

  }


  // sections of the function of source f
  if ( s < (pars.Rx + pars.R)/pars.h ){
    output_sectionXY_at_z_to_txt( pars, funcs.xs, funcs.ys, funcs.f,  0.0, "sectionXY_f_zEQ0_at_" + s_str );
    output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.f,  0.0, "sectionXZ_f_yEQ0_at_" + s_str, 0 );
  }

  }


  //
  // OUTPUT FOR THE PROBLEM ON V_Z
  //
  if ( ( pars.meth == 1 ) || ( pars.meth == 2 ) ){

  // sections of the function of source f
  if ( s < (pars.Rx + pars.R)/pars.h ){
    output_sectionXY_at_z_to_txt( pars, funcs.xs, funcs.ys, funcs.f,  0.0, "sectionXY_f_zEQ0_at_" + s_str );
    output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.f,  0.0, "sectionXZ_f_yEQ0_at_" + s_str, 0 );
  }

  // sections of v_z
  output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_z, 0.0, "sectionXZ_vz_yEQ0_at_" + s_str, 1 );
  output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_z, pars.a/2.0, "sectionXZ_vz_yEQha_at_" + s_str, 1 );
  output_sectionXZ_at_y_to_txt( pars, funcs.scur, funcs.xs, funcs.zs, funcs.v_z, pars.a, "sectionXZ_vz_yEQa_at_" + s_str, 1 );

  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_z, 0.0, "sectionXY_vz_zEQ0_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_z, pars.a, "sectionXY_vz_zEQa_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_z, pars.hf, "sectionXY_vz_zEQhf_at_" + s_str, 1 );
  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_z, pars.H, "sectionXY_vz_zEQH_at_" + s_str, 1 );

  }


  return 0;

}


int output_all_data_pp( params pars, tfuncs funcs ){

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
//	cout << "my rank is" << myrank << endl;
#endif

  int Nx = pars.Nx;
  int Ny = pars.Ny;
  int Nz = pars.Nz;

  int s = funcs.scur;

  if ( TEXTOUT >= 2 ) {
    if (i_am_verbose(myrank))
      cout << "process " << myrank << ": data output" << endl;
  }

  ostringstream sstr;
  sstr << s;
  string si_str = sstr.str();

  string s_str = si_str;

  if (s<100){
    s_str = "0" + si_str;
  }
  if (s<10){
    s_str = "00" + si_str;
  }

  ostringstream procsstr;
  procsstr << myrank;
  string proc_str = procsstr.str();



  // sections of v_z


  cout << " I AM HERE" << endl << endl;

  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_z, pars.H, "sectionXY_vz_zEQH_at_" + s_str + "_pp", 1 );

  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_x, pars.H, "sectionXY_vx_zEQH_at_" + s_str, 1 );

  output_sectionXY_at_z_to_txt( pars, funcs.scur, funcs.xs, funcs.ys, funcs.v_y, pars.H, "sectionXY_vy_zEQH_at_" + s_str, 1 );


  return 0;

}


/*
string inttostr(int n){

  //char* buf;

  //std::sprintf(buf, "%d", n);

  string str; // = string(n);


  if (n < 10 ){
    str = "0" + std::to_string(n);
  }

  return str;

}
*/


int output_border_to_txt(params pars, tfuncs funcs, string fnum){

  if ( TEXTOUT >= 3 ) {
    //cout << "start of output to TXT" << endl;
  }

  if( FILEOUT >= 1){

    ofstream myfile1;

    cout << "step in params = " << pars.step << endl;;

	myfile1.open( string("border") + fnum + string(".txt") );
    //myfile1.open( "border00.txt" );

    for( int i = 0; i < funcs.Nborder; i++ ){

      //myfile1 << funcs.border[i*2] << " " << funcs.border[i*2+1] << endl;

      myfile1 << funcs.f_at_nodes[funcs.border[i]*2] << " " << funcs.f_at_nodes[funcs.border[i]*2 + 1] << endl;

    }

    myfile1 << funcs.f_at_nodes[funcs.border[0]*2] << " " << funcs.f_at_nodes[funcs.border[0]*2 + 1] << endl;

    myfile1.close();

  }

  if ( TEXTOUT >= 2 ) {
//		output_data(exps[1]);
  }

  if ( TEXTOUT >= 3 ) {
    //cout << "end of output to TXT" << endl;
  }

  return 0;

}



int output_rect_field2d_to_txt(int Nx, int Ny, double* f, double* xs, double* ys, string fname){

  if ( TEXTOUT >= 3 ) {
    cout << "start of output to TXT" << endl;
  }

  if( FILEOUT >= 1){

    ofstream myfile1;

 	myfile1.open( fname + string(".txt") );
    //myfile1.open( "border00.txt" );

    for( int m = 0; m < Nx; m++ ){
      for( int n = 0; n < Ny; n++ ){

        myfile1 << xs[ m*(Ny) + n ] << " " << ys[ m*(Ny) + n ] << " " << f[ m*(Ny) + n ] << endl;
      }

      myfile1 << endl;
    }

    myfile1.close();

  }

  if ( TEXTOUT >= 3 ) {
    cout << "end of output to TXT" << endl;
  }

  return 0;

}


//
// img_type = 0 - as is
// img_type = 1 - circle cut
// img_type = 2 - circle cut & threshold cutoff
//
int output_sectionXY_at_z_to_txt( params pars, int s, double* xs, double* ys, double* f, double z, string fname, int img_type ){

  if ( TEXTOUT >= 3 ) {
    cout << "start of output of sectionXY to TXT" << endl;
  }

  if( FILEOUT >= 1){

    int Nx = pars.Nx;
    int Ny = pars.Ny;
    int Nz = pars.Nz;

    double x0 = pars.source_start_x + pars.a*pars.q*s;
    double y0 = pars.source_start_y + pars.b*pars.q*s;
    double z0 = pars.source_start_z + pars.c*pars.q*s;

    cur_pos(pars,&x0,&y0,&z0,s);

    int k = ceil( (z - pars.zmin)/pars.h );

    double* field;
    double* xs2d;
    double* ys2d;

    double fres;
    double xcur;
    double ycur;
    double zcur = z;

    field = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1));
    xs2d = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1));
    ys2d = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1));

    for( int m = 0; m < Nx+1; m++ ){
      for( int n = 0; n < Ny+1; n++ ){

          fres = f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
          xcur = xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
          ycur = ys[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

          // 0 - as is
          // 1 - circle cut
          // 2 - circle cut & threshold cutoff
          if (( img_type == 1 )||( img_type == 2 )) {

            if ( (xcur-x0)*(xcur-x0) + (ycur-y0)*(ycur-y0) + (zcur-z0)*(zcur-z0) < pars.R*pars.R ){
              fres = 0.0;
            }

          }

          if( img_type == 2 ){

            if ( fres > pars.theta )
              fres = pars.theta;

            if ( fres < -pars.theta )
              fres = -pars.theta;

          }

          field[ m*(Ny+1) + n ] = fres;
          xs2d[ m*(Ny+1) + n ]  = xcur;
          ys2d[ m*(Ny+1) + n ]  = ycur;

      }
    }

    output_rect_field2d_to_txt( pars.Nx + 1, pars.Ny + 1, field, xs2d, ys2d, fname);

  }else{
    cout << "TXT file was not created" << endl;
  }

  if ( TEXTOUT >= 3 ) {
    cout << "end of output of sectionXY to TXT" << endl;
  }

  return 0;

}

//
// 0 - as is
// 1 - circle cut
// 2 - circle cut & threshold cutoff
int output_2dFunctionOfXY_to_txt( params pars, double s, double* xs, double* ys, double* f, string fname, int img_type, double flat ){

  if ( TEXTOUT >= 3 ) {
    cout << "start of output of sectionXY to TXT" << endl;
  }

  if( FILEOUT >= 1){

    int Nx = pars.Nx;
    int Ny = pars.Ny;
    int Nz = pars.Nz;

    double x0 = pars.source_start_x + pars.a*pars.q*s;
    double y0 = pars.source_start_y + pars.b*pars.q*s;
    double z0 = pars.source_start_z + pars.c*pars.q*s;

    cur_pos(pars,&x0,&y0,&z0,s);

    double* field;
    double* xs2d;
    double* ys2d;

    double fres;
    double xcur;
    double ycur;

    field = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1));
    xs2d = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1));
    ys2d = (double*)malloc(sizeof(double)*(Nx+1)*(Ny+1));

    for( int m = 0; m < Nx+1; m++ ){
      for( int n = 0; n < Ny+1; n++ ){

          fres = f[ m*(Ny+1) + n ];
          xcur = xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + 0 ];
          ycur = ys[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + 0 ];

          // 0 - as is
          // 1 - circle cut
          // 2 - circle cut & threshold cutoff
          if (( img_type == 1 )||( img_type == 2 )) {

            if ( (xcur-x0)*(xcur-x0) + (ycur-y0)*(ycur-y0) < pars.R*pars.R ){
              fres = flat;
            }

          }

          if( img_type == 2 ){

            if ( fres > pars.theta )
              fres = pars.theta;

            if ( fres < -pars.theta )
              fres = -pars.theta;

          }

          field[ m*(Ny+1) + n ] = fres;
          xs2d[ m*(Ny+1) + n ]  = xcur;
          ys2d[ m*(Ny+1) + n ]  = ycur;

      }
    }

    output_rect_field2d_to_txt( pars.Nx + 1, pars.Ny + 1, field, xs2d, ys2d, fname);

  }else{
    cout << "TXT file was not created" << endl;
  }

  if ( TEXTOUT >= 3 ) {
    cout << "end of output of sectionXY to TXT" << endl;
  }

  return 0;

}


int output_sectionXZ_N_txt( params pars, double* xs, double* zs, string fname ){

  if ( TEXTOUT >= 3 ) {
    cout << "start of output of N(x,z) to TXT" << endl;
  }

  if( FILEOUT >= 1){

    int Nx = pars.Nx;
    int Ny = pars.Ny;
    int Nz = pars.Nz;

    double y = 0.0;

    //int m = ceil( (x - pars.xmin)/pars.h );
    int n = ceil( (y - pars.ymin)/pars.h );
    //int k = ceil( (z - pars.zmin)/pars.h );

    double* field;
    double* xs2d;
    double* zs2d;

    double fres;
    double xcur;
    double ycur = y;
    double zcur;


    field = (double*)malloc(sizeof(double)*(Nx+1)*(Nz+1));
    xs2d = (double*)malloc(sizeof(double)*(Nx+1)*(Nz+1));
    zs2d = (double*)malloc(sizeof(double)*(Nx+1)*(Nz+1));

    for( int m = 0; m < Nx+1; m++ ){
      for( int k = 0; k < Nz+1; k++ ){

          xcur = xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
          zcur = zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
          fres = Nfunc(pars, zcur);

          field[ m*(Nz+1) + k ] = fres;
          xs2d[ m*(Nz+1) + k ]  = xcur;
          zs2d[ m*(Nz+1) + k ]  = zcur;

      }
    }

    output_rect_field2d_to_txt( pars.Nx + 1, pars.Nz + 1, field, xs2d, zs2d, fname);

  }else{
    cout << "TXT file was not created" << endl;
  }

  if ( TEXTOUT >= 3 ) {
    cout << "end of output of N(x,z) to TXT" << endl;
  }

  return 0;

}

//
// img_type = 0 - as is
// img_type = 1 - circle cut
// img_type = 2 - circle cut & threshold cutoff
//
int output_sectionXZ_at_y_to_txt( params pars, int s, double* xs, double* zs, double* f, double y, string fname, int img_type ){

  if ( TEXTOUT >= 3 ) {
    cout << "start of output of sectionXZ to TXT" << endl;
  }

  if( FILEOUT >= 1){

    int Nx = pars.Nx;
    int Ny = pars.Ny;
    int Nz = pars.Nz;

    double x0 = pars.source_start_x + pars.a*pars.q*s;
    double y0 = pars.source_start_y + pars.b*pars.q*s;
    double z0 = pars.source_start_z + pars.c*pars.q*s;

    cur_pos(pars,&x0,&y0,&z0,s);

    //int m = ceil( (x - pars.xmin)/pars.h );
    int n = ceil( (y - pars.ymin)/pars.h );
    //int k = ceil( (z - pars.zmin)/pars.h );

    double* field;
    double* xs2d;
    double* zs2d;

    double fres;
    double xcur;
    double ycur = y;
    double zcur;


    field = (double*)malloc(sizeof(double)*(Nx+1)*(Nz+1));
    xs2d = (double*)malloc(sizeof(double)*(Nx+1)*(Nz+1));
    zs2d = (double*)malloc(sizeof(double)*(Nx+1)*(Nz+1));

    for( int m = 0; m < Nx+1; m++ ){
      for( int k = 0; k < Nz+1; k++ ){

          fres = f[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
          xcur = xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
          zcur = zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

          // 0 - as is
          // 1 - circle cut
          // 2 - circle cut & threshold cutoff
          if (( img_type == 1 )||( img_type == 2 )) {

            if ( (xcur-x0)*(xcur-x0) + (ycur-y0)*(ycur-y0) + (xcur-x0)*(xcur-x0) + (zcur-z0)*(zcur-z0) < pars.R*pars.R ){
              fres = 0.0;
            }

          }

          if( img_type == 2 ){

            if ( fres > pars.theta )
              fres = pars.theta;

            if ( fres < -pars.theta )
              fres = -pars.theta;

          }

          field[ m*(Nz+1) + k ] = fres;
          xs2d[ m*(Nz+1) + k ]  = xcur;
          zs2d[ m*(Nz+1) + k ]  = zcur;

      }
    }

    output_rect_field2d_to_txt( pars.Nx + 1, pars.Nz + 1, field, xs2d, zs2d, fname);

  }else{
    cout << "TXT file was not created" << endl;
  }

  if ( TEXTOUT >= 3 ) {
    cout << "end of output of sectionXZ to TXT" << endl;
  }

  return 0;

}


int output_vector_field2d_to_txt(int N, double* xs, double* ys, double* vx, double* vy, string fname){

  if ( TEXTOUT >= 3 ) {
    cout << "start of output to TXT" << endl;
  }

  if( FILEOUT >= 1){

    ofstream myfile1;

 	myfile1.open( fname + string(".txt") );
    //myfile1.open( "border00.txt" );

    for( int m = 0; m < N; m++ ){

        myfile1 << xs[ m ] << " " << ys[ m ] << " " << vx[ m ] << " " << vy[ m ] << endl;

    }

    myfile1.close();

  }

  if ( TEXTOUT >= 3 ) {
    cout << "end of output to TXT" << endl;
  }

  return 0;

}


int output_vfieldXZ_at_y_to_txt_cutARoundSource( params pars, int s, double* xs, double* zs, double* vx, double* vy, double y, string fname, int img_type, double hor_size, double vert_size, double xshift ){

  if ( TEXTOUT >= 3 ) {
    cout << "start of output of vector field of v in XZ-plane to TXT" << endl;
  }

  if( FILEOUT >= 1){

    int Nx = pars.Nx;
    int Ny = pars.Ny;
    int Nz = pars.Nz;

    double x0 = pars.source_start_x + pars.a*pars.q*s;
    double y0 = pars.source_start_y + pars.b*pars.q*s;
    double z0 = pars.source_start_z + pars.c*pars.q*s;

    cur_pos(pars,&x0,&y0,&z0,s);

    //int m = ceil( (x - pars.xmin)/pars.h );
    int n = ceil( (y - pars.ymin)/pars.h );
    //int k = ceil( (z - pars.zmin)/pars.h );

    double* vectx;
    double* vecty;
    double* xs2d;
    double* zs2d;

    double fres;
    double xcur;
    double ycur = y;
    double zcur;

    int nstep = (int)( pars.vect_space/pars.h );
    cout << "nstep = " << nstep << endl;

    int Nxsize = Nx/nstep + 2;
    int Nysize = Nz/nstep + 2;
    vectx = (double*)malloc(sizeof(double)*(Nxsize)*(Nysize));
    vecty = (double*)malloc(sizeof(double)*(Nxsize)*(Nysize));
    xs2d = (double*)malloc(sizeof(double)*(Nxsize)*(Nysize));
    zs2d = (double*)malloc(sizeof(double)*(Nxsize)*(Nysize));

    int p = 0;
    double modcur = 0.0;
    double modmax = 0.0;



    // 0 - as is
    // 1 - circle cut
    // 2 - circle cut & scale

    for( int m = 0; m < Nx+1; m += nstep ){
      for( int k = 0; k < Nz+1; k += nstep ){

          xcur = xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
          zcur = zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

          if( ( abs(xcur - x0 - xshift) < hor_size/2.0 ) &&( abs(zcur - z0) < vert_size/2.0 ) ){

            if ( (img_type == 1 )|| ( (xcur-x0 -xshift)*(xcur-x0 -xshift) + (ycur-y0)*(ycur-y0) + (xcur-x0 -xshift)*(xcur-x0 -xshift) + (zcur-z0)*(zcur-z0) > pars.vect_Rvect*pars.vect_Rvect ) ){

              vectx[ p ] = vx[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ]; // - pars.a;
              vecty[ p ] = vy[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ]; // - pars.c;
              xs2d[ p ]  = xcur;
              zs2d[ p ]  = zcur;

              modcur = sqrt( vectx[ p ]*vectx[ p ] + vecty[ p ]*vecty[ p ] );
              if( modmax < modcur )
                modmax = modcur;

              p++;

            }

         }

      }
    }

    if ( img_type == 2 ) {
      for( int m = 0; m < p; m++ ){

        vectx[ m ] /= modmax;
        vecty[ m ] /= modmax;
        vectx[ m ] *= pars.vect_scale;
        vecty[ m ] *= pars.vect_scale;

      }
    }

    output_vector_field2d_to_txt( p, xs2d, zs2d, vectx, vecty, fname );

  }else{
    cout << "TXT file was not created" << endl;
  }

  if ( TEXTOUT >= 3 ) {
    cout << "end of output of sectionXZ to TXT" << endl;
  }

  return 0;

}

int output_vfieldXZ_at_y_to_txt( params pars, int s, double* xs, double* zs, double* vx, double* vy, double y, string fname, int img_type, int nstep ){

  if ( TEXTOUT >= 3 ) {
    cout << "start of output of vector field of v in XZ-plane to TXT" << endl;
  }

  if( FILEOUT >= 1){

    int Nx = pars.Nx;
    int Ny = pars.Ny;
    int Nz = pars.Nz;

    double x0 = pars.source_start_x + pars.a*pars.q*s;
    double y0 = pars.source_start_y + pars.b*pars.q*s;
    double z0 = pars.source_start_z + pars.c*pars.q*s;

    cur_pos(pars,&x0,&y0,&z0,s);

    //int m = ceil( (x - pars.xmin)/pars.h );
    int n = ceil( (y - pars.ymin)/pars.h );
    //int k = ceil( (z - pars.zmin)/pars.h );

    double* vectx;
    double* vecty;
    double* xs2d;
    double* zs2d;

    double fres;
    double xcur;
    double ycur = y;
    double zcur;

    cout << "nstep = " << nstep << endl;

    int Nxsize = Nx/nstep + 2;
    int Nysize = Nz/nstep + 2;
    vectx = (double*)malloc(sizeof(double)*(Nxsize)*(Nysize));
    vecty = (double*)malloc(sizeof(double)*(Nxsize)*(Nysize));
    xs2d = (double*)malloc(sizeof(double)*(Nxsize)*(Nysize));
    zs2d = (double*)malloc(sizeof(double)*(Nxsize)*(Nysize));

    int p = 0;
    double modcur = 0.0;
    double modmax = 0.0;



    // 0 - as is
    // 1 - circle cut
    // 2 - circle cut & scale

    for( int m = 0; m < Nx+1; m += nstep ){
      for( int k = 0; k < Nz+1; k += nstep ){

          xcur = xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
          zcur = zs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

          if ( (img_type == 1 )|| ( (xcur-x0)*(xcur-x0) + (ycur-y0)*(ycur-y0) + (xcur-x0)*(xcur-x0) + (zcur-z0)*(zcur-z0) > pars.vect_Rvect*pars.vect_Rvect ) ){

            vectx[ p ] = vx[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ]; // - pars.a;;
            vecty[ p ] = vy[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
            xs2d[ p ]  = xcur;
            zs2d[ p ]  = zcur;

            modcur = sqrt( vectx[ p ]*vectx[ p ] + vecty[ p ]*vecty[ p ] );
            if( modmax < modcur )
              modmax = modcur;

            p++;

          }

      }
    }

    if ( img_type == 2 ) {
      for( int m = 0; m < p; m++ ){

        vectx[ m ] /= modmax;
        vecty[ m ] /= modmax;
        vectx[ m ] *= pars.vect_scale;
        vecty[ m ] *= pars.vect_scale;

      }
    }

    output_vector_field2d_to_txt( p, xs2d, zs2d, vectx, vecty, fname );

  }else{
    cout << "TXT file was not created" << endl;
  }

  if ( TEXTOUT >= 3 ) {
    cout << "end of output of sectionXZ to TXT" << endl;
  }

  return 0;

}


int output_vfieldXY_at_z_to_txt( params pars, int s, double* xs, double* ys, double* vx, double* vy, double z, string fname, int img_type, int nstep, double scale ){

  if ( TEXTOUT >= 3 ) {
    cout << "start of output of vector field of v in XZ-plane to TXT" << endl;
  }

  if( FILEOUT >= 1){

    int Nx = pars.Nx;
    int Ny = pars.Ny;
    int Nz = pars.Nz;

    double x0 = pars.source_start_x + pars.a*pars.q*s;
    double y0 = pars.source_start_y + pars.b*pars.q*s;
    double z0 = pars.source_start_z + pars.c*pars.q*s;

    cur_pos(pars,&x0,&y0,&z0,s);

    //int m = ceil( (x - pars.xmin)/pars.h );
    //int n = ceil( (y - pars.ymin)/pars.h );
    int k = ceil( (z - pars.zmin)/pars.h );
    cout << "k = " << k << endl;

    double* vectx;
    double* vecty;
    double* xs2d;
    double* ys2d;

    double fres;
    double xcur;
    double ycur;
    double zcur = z;

    //int nstep = (int)( pars.vect_space/pars.h );
    cout << "nstep = " << nstep << endl;

    int Nxsize = Nx/nstep + 2;
    int Nysize = Nz/nstep + 2;
    vectx = (double*)malloc(sizeof(double)*(Nxsize)*(Nysize));
    vecty = (double*)malloc(sizeof(double)*(Nxsize)*(Nysize));
    xs2d = (double*)malloc(sizeof(double)*(Nxsize)*(Nysize));
    ys2d = (double*)malloc(sizeof(double)*(Nxsize)*(Nysize));

    int p = 0;
    double modcur = 0.0;
    double modmax = 0.0;



    // 0 - as is
    // 1 - circle cut
    // 2 - circle cut & scale

    for( int m = 0; m < Nx+1; m += nstep ){
      for( int n = 0; n < Nz+1; n += nstep ){

          xcur = xs[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
          ycur = ys[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];

          if ( (img_type == 0 )|| ( (xcur-x0)*(xcur-x0) + (ycur-y0)*(ycur-y0) + (xcur-x0)*(xcur-x0) + (zcur-z0)*(zcur-z0) > pars.vect_Rvect*pars.vect_Rvect ) ){

            vectx[ p ] = vx[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ]; // - pars.a;;
            vecty[ p ] = vy[ m*(Ny+1)*(Nz+1) + n*(Nz+1) + k ];
            xs2d[ p ]  = xcur;
            ys2d[ p ]  = ycur;

            modcur = sqrt( vectx[ p ]*vectx[ p ] + vecty[ p ]*vecty[ p ] );
            if( modmax < modcur )
              modmax = modcur;

            p++;

          }

      }
    }

    if ( img_type == 2 ) {
      for( int m = 0; m < p; m++ ){

        vectx[ m ] /= modmax;
        vecty[ m ] /= modmax;
        vectx[ m ] *= scale;
        vecty[ m ] *= scale;

      }
    }

    output_vector_field2d_to_txt( p, xs2d, ys2d, vectx, vecty, fname );

  }else{
    cout << "TXT file was not created" << endl;
  }

  if ( TEXTOUT >= 3 ) {
    cout << "end of output of sectionXY to TXT" << endl;
  }

  return 0;

}


int output_sectionXY_at_z_to_txt( params pars, double* xs, double* ys, double* f, double z, string fname, int img_type ){

  if ( TEXTOUT >= 3 ) {
    cout << "start of output of sectionXY to TXT" << endl;
  }

  if ( ( img_type == 1 )||( img_type == 2 ) ){

    if ( TEXTOUT >= 1 ) {
      cout << "Error at output_sectionXY_at_z_to_txt: You should specify iteration when calling the function with img_type equal to 1 or 2" << endl;
    }

    return -1;
  }

  output_sectionXY_at_z_to_txt( pars, -1, xs, ys, f, z, fname, img_type );

  if ( TEXTOUT >= 3 ) {
    cout << "end of output of sectionXY to TXT" << endl;
  }

  return 0;

}


int output_sectionXY_at_z_to_txt( params pars, double* xs, double* ys, double* f, double z, string fname ){

  if ( TEXTOUT >= 3 ) {
    cout << "start of output of sectionXY to TXT" << endl;
  }

  output_sectionXY_at_z_to_txt( pars, -1, xs, ys, f, z, fname, 0 );

  if ( TEXTOUT >= 3 ) {
    cout << "end of output of sectionXY to TXT" << endl;
  }

  return 0;

}


int destruct_all( params pars, tfuncs funcs ){

  gsl_spmatrix_free( funcs.Q );
  gsl_spmatrix_free( funcs.Qc );
  gsl_vector_free( funcs.b );
  gsl_vector_free( funcs.x );

  return 0;

}

int calc_current(){

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

  params pars;
  tfuncs funcs;
  params_pp pars_pp;
  tfuncs_pp funcs_pp;

  init_params( &pars, &funcs, &pars_pp );

  if (( TEXTOUT >= 2 )&&(i_am_verbose(myrank))) {

    cout << "in process number " << myrank << ":";

    print_params_pp( pars_pp );
    print_params( pars, funcs);
  }

 init_funcs( pars, &funcs );



  process_funcs( pars, funcs, pars_pp, funcs_pp );

  //output_border_to_txt( pars, funcs, "" );

  //output_sectionXY_at_z_to_txt( pars, funcs.xs, funcs.ys, funcs.f, 0.0, "sectionXY_f_at_0" );

  destruct_all( pars, funcs );

  return 0;

}

int load_test_u( params pars, tfuncs funcs ){

  return 0;

}

int self_test( params pars, tfuncs funcs, params_pp pars_pp ){

  //
  cout<< "TEST: loaded parameter values" << endl;

  init_params( &pars, &funcs, &pars_pp );
  init_funcs( pars, &funcs );

  print_params( pars, funcs );


  //
  cout<< "TEST: u postrocessing" << endl;
  funcs.scur = 0;
  process_funcs_prepare( pars, funcs );

  load_test_u( pars, funcs );

  obtain_current_fields( pars, funcs );

  output_all_data( pars, funcs );

  //
  // main calculation cycle
  //
  for( int s = 0; s < pars.smax; s++ ){

    if ( TEXTOUT >= 2 ) {
      cout << "calculating at step " << s << endl;
    }

    process_funcs_oneStep( pars, funcs );
    funcs.scur = s + 1;

    obtain_current_fields( pars, funcs );

    output_all_data( pars, funcs );

    u_shift( pars, funcs );

  }

  return 0;

}



int run(int argc, char **argv){

	//using namespace std;

    //cout << "Armadillo version: " << arma_version::as_string() << endl;

	int myrank = 0, np = 1;


#if GO_MPI == 1
	MPI_Init(&argc, &argv);
#endif

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
//	cout << "my rank is" << myrank << endl;
#endif

	if ( TEXTOUT >= 1)
	if ( myrank == 0 )
	  cout << "number of processes = " << np << endl << endl;

	double aclock1, aclock2;

	aclock1 = mclock();

    //gsl_set_error_handler_off();

	calc_current();

	aclock2 = mclock();

	if ( TEXTOUT >= 0){
	  if (myrank == 0){
	    cout << "Total time: " << diffclock(aclock1, aclock2) << "s" << endl << endl;
	  }
	}


#if GO_MPI == 1
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
#endif



	return 0;

}

int main(int argc, char **argv)
{


	//printf("Starting...\n");

	int res;

	cout << endl << "Starting post processing..." << endl << endl;

	res = run(argc,argv);

	//printf("returned code: %d\n", res);

	//int s;
	//scanf("%d", &s);

	cout << endl << "Program ended with code " << res << endl << endl;

    return 0;
}
