#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <gsl/gsl_math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_spmatrix.h>
#include <gsl/gsl_splinalg.h>

//#define M_PI (3.14159265358979323846264338327950288)

// TEXTOUT
// -1 - no output to stdou at all
// 0 - prints only total time
// 1 - output to stdout of system (basic) information + errors
// 2 - output to stdout of the whole workflow + errors
// 3 - output to stdout of the whole workflow and debug text + errors
#define TEXTOUT 2

// FILEOUT
// 0 - no file output
// 1 - basic file output
// 2 - extended file output
// 3 - debug file output
#define FILEOUT 1

#define GO_MPI 0


#if GO_MPI == 1

#include <mpi.h>

#endif

struct params_pp{

  double t1;  // time start
  string t1_disc;

  double t2;  // time finish
  string t2_disc;

  int Nt;  // points in time
  string Nt_disc;

  double x1; // x-coordinate of left bottom corner
  string x1_disc;

  double y1; // y-coordinate of left bottom corner
  string y1_disc;

  double x2; // x-coordinate of right upper corner
  string x2_disc;

  double y2; // y-coordinate of right upper corner
  string y2_disc;

  double q; //(t2-t1)/Nt; // step in time
  string q_disc;

  double h; // step along all axes
  string h_disc;

  double g; // gravity acceleration
  string g_disc;

  int smax; // number of iterations
  string smax_disc;

};

struct params{

	double gamma; // constant

	int Nsteps;
	int step;

	double t; // time
	double dt; // time interval

	int meth; // problem formulation
	            // 0 - equation for potential \psi:  L\psi = ...
	            // 1 - equation for velocity component v_z:  Lv_z = ...
	            // 2 - equation for velocity component v_z:  Lv_z = ..., conditions of free surface: dtdtdz v_z = g \Delta_xy v_z
    string meth_disc;

    int meth_fs_bc; // boundary conditions of free surface
                         // 0 - u_z -   3u(k)-4u(k-1)+1u(k-2) / 2h
                         // 1 - u_z -   u(k)-u(k-1) / h
    string meth_fs_bc_disc;





  double H;  // half of the depth
  string H_disc;

  int Nz;  // points along z-axis
  string Nz_disc;

  double solver_tol;  /* solution relative tolerance */
  string solver_tol_disc;

  int solver_max_iter; /* maximum iterations */
  string solver_max_iter_disc;

  double rx;  // width/height rate
  string rx_disc;

  double ry;  // length/height rate
  string ry_disc;

  double a; // x-coord of the velocity of the source
  string a_disc;

  double b; // y-coord of the velocity of the source
  string b_disc;

  double c; // z-coord of the velocity of the source
  string c_disc;

  double N; // buoyancy frequency
  string N_disc;

  double r; // buoyancy frequency
  string r_disc;

  double rho0; // density of the liquid
  string rho0_disc;

  double hf; // z=hf - plane of free surface
  string hf_disc;

  double p0; // atmospheric pressure
  string p0_disc;

  double g; // gravity acceleration
  string g_disc;

  double h; // step along all axes
  string h_disc;

  double q; // step in time
  string q_disc;

  double Rx; // width
  string Rx_disc;

  double Ry; // length
  string Ry_disc;

  double source_start_x; // start x-coordinate of the source
  string source_start_x_disc;

  double source_start_y; // start y-coordinate of the source
  string source_start_y_disc;

  double source_start_z; // start z-coordinate of the source
  string source_start_z_disc;

  int Nx; // points along x-axis
  string Nx_disc;

  int Ny; // points along y-axis
  string Ny_disc;

  double xmin; // minimum of x coordinate
  string xmin_disc;

  double xmax; // maximum of x coordinate
  string xmax_disc;

  double ymin; // minimum of y coordinate
  string ymin_disc;

  double ymax; // maximum of y coordinate
  string ymax_disc;

  double zmin; // minimum of z coordinate
  string zmin_disc;

  double zmax; // maximum of z coordinate
  string zmax_disc;

  double tmax; // duration of movement
  string tmax_disc;

  double p; // ratio of mass of the source that lays inside the cutoff circle
  string p_disc;

  double theta; // threshold when displaying output fields as images
  string theta_disc;

  double R; // radius for cutoff
  string R_disc;

  double Rprec; // precision when finding radius for cutoff
  string Rprec_disc;

  double vect_space; // space between vector start points
  string vect_space_disc;

  double vect_scale; // space between vector start points
  string vect_scale_disc;

  double vect_Rvect; // vector field cutoff
  string vect_Rvect_disc;

  //int smax = 8; // number of iterations
  int smax; // number of iterations
  string smax_disc;

  double A; // parameter of the Gaussian function f
  string A_disc;

  double B; // parameter of the Gaussian function f
  string B_disc;

  double Lon; // parameter of the source function multiplyer function f_in, distance where f_in = 0
  string Lon_disc = "parameter of the source multiplier f_in";

  double Lin; // parameter of the source function multiplyer function f_in, distance where f_in linearly increases
  string Lin_disc = "parameter of the source multiplier f_in";

  double sinLam; // parameter of the sinusoidal path, wavelength
  string sinLam_disc = "parameter of the sinusoidal path, wavelength";

  double sinD; // parameter of the sinusoidal path, amplitude
  string sinD_disc = "parameter of the sinusoidal path, amplitude";


  // MPI section

  int w;
  int L;
  int Na;
  int sa;
  int out_freq;


};



struct tfuncs{
	double* nodes; // D
	int* border; //
	double* f_at_nodes;
	int Nborder;
	int Nnodes;

  int scur;

  double* f; //  [Nx+1,Ny+1,Nz+1] - values of the function that represents the source
  double* xs; // [Nx+1,Ny+1,Nz+1] - x-coordinates of the computation grid
  double* ys; // [Nx+1,Ny+1,Nz+1] - y-coordinates of the computation grid
  double* zs; // [Nx+1,Ny+1,Nz+1] - z-coordinates of the computation grid]

  double* BC_top; // BC at the top
  double* BC_bottom; // BC at the bottom

  double* psi; // [Nx+1,Ny+1,Nz+1,4] - inner potential
  double* dpsi_dx; // [Nx+1,Ny+1,Nz+1,4] - x-derivative of the inner potential
  double* dpsi_dy; // [Nx+1,Ny+1,Nz+1,4] - y-derivative of the inner potential
  double* dpsi_dz; // [Nx+1,Ny+1,Nz+1,4] - z-derivative of the inner potential
  double* zeta; // [Nx+1,Ny+1,Nz+1] - vertical displacement
  double* hatzeta; // [Nx+1,Ny+1,Nz+1] - normalized vertical displacement

  double* v_x; // [Nx+1,Ny+1,Nz+1] - x-component of the velocity
  double* v_y; // [Nx+1,Ny+1,Nz+1] - y-component of the velocity
  double* v_z; // [Nx+1,Ny+1,Nz+1] - z-component of the velocity

  double* v_x_mem; // [Nx+1,Ny+1,Nz+1] - 4 last steps of x-component of the velocity
  double* v_y_mem; // [Nx+1,Ny+1,Nz+1] - 4 last steps of y-component of the velocity

  double* dv_z_dx; // [Nx+1,Ny+1,Nz+1] - x-derivative of z-component of the velocity
  double* dv_z_dy; // [Nx+1,Ny+1,Nz+1] - y-derivative of z-component of the velocity

  double* v_x_an; // [Nx+1,Ny+1,Nz+1] - x-component of the velocity of the analytic approximation
  double* v_y_an; // [Nx+1,Ny+1,Nz+1] - y-component of the velocity of the analytic approximation
  double* v_z_an; // [Nx+1,Ny+1,Nz+1] - z-component of the velocity of the analytic approximation

  double* p; // [Nx+1,Ny+1,Nz+1] - pressure
  double* zeta_sp; // [Nx+1,Ny+1,Nz+1] - free surface around z=h_f

  double* u; // [Nx+1,Ny+1,Nz+1,3] -  velocity of fluid

  double* uws_at_proc0; // [Nx+1,Ny+1,Nz+1,3,~L]
  int* Nzs_at_proc0; //

  double* Omega_w_plus_SEND;
  double* Omega_w_plus_RECV;
  double* Omega_w_minus_SEND;
  double* Omega_w_minus_RECV;


  gsl_spmatrix* Q; // matrix of SLAE - triplet format
  gsl_spmatrix* Qc; // matrix of SLAE - compressed format
  gsl_vector* b;  // right hand side vector
  gsl_vector *x;    // solution vector

  int PATTERN_SIZE;

};


struct tfuncs_pp{

  int scur;

};


int run(int, char**);

int init_params( params*, tfuncs*, params_pp* );

//int init_params( params*, tfuncs* );

int output_border_to_txt(params, tfuncs, string);

int output_sectionXY_at_z_to_txt( params, double*, double*, double*, double, string );

int output_sectionXY_at_z_to_txt( params, double*, double*, double*, double, string, int );

int output_sectionXY_at_z_to_txt( params, int, double*, double*, double*, double, string, int );

int output_sectionXZ_at_y_to_txt( params, int, double*, double*, double*, double, string, int );

int output_sectionXZ_N_txt( params, double*, double*, string );

int output_all_data( params, tfuncs );

int output_all_data_pp( params, tfuncs );

int output_2dFunctionOfXY_to_txt( params, double, double*, double*, double*, string, int, double );

int output_vfieldXZ_at_y_to_txt( params, int, double*, double*, double*, double*, double, string, int, int );
int output_vfieldXY_at_z_to_txt( params, int, double*, double*, double*, double*, double, string, int, int, double );

int output_vfieldXZ_at_y_to_txt_cutARoundSource( params, int, double*, double*, double*, double*, double, string, int, double, double, double );
