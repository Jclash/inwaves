


rs-pp-vx-xyplane-csection
[постобработка inwaves]
Построение компоненты v_x скорости жидкости в центральном сечении в плоскости XY на расстоянии hf по вертикали от источника (т.е. вдоль линии z=hf, y=0) 




vfield-analyt-sfps
- Поле скоростей в толще жидкости при обтекании горизонтальным потоком идеальной стратифицированной жидкости точечного источника (полубесконечного тела)
sfps - stratified fluid, point source

vfield-analyt-sfps-xz-python.py
- Поле скоростей в толще жидкости при обтекании горизонтальным потоком идеальной стратифицированной жидкости точечного источника (полубесконечного тела), сечение в плоскости xz

vfield-analyt-ifps
Поле скоростей в толще жидкости при обтекании горизонтального потока идеальной жидкости точечного источника (полубесконечного тела)
[расчет и постобработка]

ifps - ideal fluid [flows around] point source


rs-pp-intcurves
Построение поля направлений и интегральных кривых
[постобработка inwaves]

intcurves-job.txt
имена файлов для обработки

intcurves.py
rs-pp-intcurves
rs-pp-intcurves-calc
rs-pp-intcurves-copy
rs-pp-intcurves-plot
vfield_v_XZ_curves.inp

Центральное сечение свободной поверхности
[постобработка inwaves]

rs-pp-csection
rs-pp-csection-calc
rs-pp-csection-copy
rs-pp-csection-gnuplot.inp
rs-pp-csection-job.txt
rs-pp-csection-plot
rs-pp-csection.py


Поле скоростей на свободной поверхности при обтекании горизонтального потока идеальной жидкости точечного источника (полубесконечного тела)
[расчет и постобработка]

vfield-analyt-ifs
vfield-analyt-ifs-calc
vfield-analyt-ifs-gp-csection.inp
vfield-analyt-ifs-gp-vfield.inp
vfield-analyt-ifs-plot
vfield-analyt-ifs-plot-cs
vfield-analyt-ifs-python.py
vfield-analyt-ifs-wm.nb

