from numpy import loadtxt
import sys
import math
import numpy


Nx = 31
Ny = 31

Rx = 30.0
Ry = 30.0

E = 1.0
h = 4.0
R = 1.0
g = 9.81

V = math.sqrt(g*R/E)
Q = math.pi*V*R*R

intMax = 10.0
h1d = 0.01
h2d = 0.1

nFactor = 20.0
Vx0 = V

xmin = -Rx/2.0
xmax = Rx/2.0
ymin = -Rx/2.0
ymax = Rx/2.0

stepx = (xmax - xmin)/(Nx - 1)
stepy = (ymax - ymin)/(Ny - 1)

alpha = g*Q/(math.pi*math.pi*V*V)
beta = g*Q/math.pi/V/V
nu = g/V/V


print('Rx =',Rx)
print('Ry =',Ry)
print('V =',V)
print('h =',h)
print('Q =',Q)
print('g =',g)
print('E =',E)
print('R =',R)
print('alpha =',alpha)
print('beta =',beta)
print('nu =',nu)
print('Nx =',Nx)
print('Ny =',Ny)
print('intMax =',intMax)
print('h1d =',h1d)
print('h2d =',h2d)
print('nFactor =',nFactor)
print('Vx0 =',Vx0)


R_obj = math.sqrt(Q/math.pi/V)
print('Radius of the object =',R_obj)


def Ifunc(x, y, sigma, lam):
  return math.cos(lam*y)*math.exp(-abs(x)*math.sqrt(sigma*sigma+lam*lam))*(nu*sigma*math.cos(sigma*h)+(sigma*sigma+lam*lam)*math.sin(sigma*h))*sigma/((sigma*sigma+lam*lam)*(sigma*sigma+lam*lam)+nu*nu*sigma*sigma)


def Iint( x, y ):
  sigma_max = intMax
  lam_max = intMax
  h_sigma = h2d
  h_lam = h2d

  N_sigma = int(sigma_max/h_sigma)
  N_lam = int(lam_max/h_lam)

  res = 0.0

  sigmas = [x*sigma_max / N_sigma for x in range(N_sigma)]
  #print(sigmas)
  lams = [x*lam_max / N_lam for x in range(N_lam)]
  #print(lams)

  for lam in lams:
    for sigma in sigmas:
      if( sigma*lam != 0.0):
        res += Ifunc(x, y, sigma, lam)*h_sigma*h_lam

  return res


def Jfunc(x, y, gamma):
  return math.sqrt(gamma/(gamma-nu))*math.exp(-h*gamma)*math.cos(x*math.sqrt(nu*gamma))*math.cos(y*math.sqrt(gamma*(gamma-nu)))


def Jint( x, y ):
  gamma_max = intMax
  h_gamma = h1d

  N_gamma = int((gamma_max-nu)/h_gamma)

  res = 0.0


  gammas = [nu+x*h_gamma for x in range(N_gamma)]
  #print(gammas)

  for gamma in gammas:
    if( gamma - nu != 0.0):
      res += Jfunc(x, y, gamma)*h_gamma

  return res


def Kfunc(x, y, sigma, lam):
  return math.sin(lam*y)*math.exp(-abs(x)*math.sqrt(sigma*sigma+lam*lam))*(nu*sigma*math.cos(sigma*h)+(sigma*sigma+lam*lam)*math.sin(sigma*h))*sigma*lam/((sigma*sigma+lam*lam)*(sigma*sigma+lam*lam)+nu*nu*sigma*sigma)/math.sqrt(sigma*sigma+lam*lam)


def Kint( x, y ):
  sigma_max = intMax
  lam_max = intMax
  h_sigma = h2d
  h_lam = h2d

  N_sigma = int(sigma_max/h_sigma)
  N_lam = int(lam_max/h_lam)

  res = 0.0

  sigmas = [x*sigma_max / N_sigma for x in range(N_sigma)]
  #print(sigmas)
  lams = [x*lam_max / N_lam for x in range(N_lam)]
  #print(lams)

  for lam in lams:
    for sigma in sigmas:
      if( sigma*lam != 0.0):
        res += Kfunc(x, y, sigma, lam)*h_sigma*h_lam

  return res


def Mfunc(x, y, gamma):
  return math.sqrt(nu*gamma)*math.exp(-h*gamma)*math.sin(x*math.sqrt(nu*gamma))*math.sin(y*math.sqrt(gamma*(gamma-nu)))


def Mint( x, y ):
  gamma_max = intMax
  h_gamma = h1d

  N_gamma = int((gamma_max-nu)/h_gamma)

  res = 0.0


  gammas = [nu+x*h_gamma for x in range(N_gamma)]
  #print(gammas)

  for gamma in gammas:
    res += Mfunc(x, y, gamma)*h_gamma

  return res

vfield = []
vfield_minus = []
vfield_minus_normed = []
csection = []

for i in range(Nx):
  for j in range(Ny):
    x = xmin + i*stepx
    y = ymin + j*stepy

    I = 0.0
    J = 0.0
    K = 0.0
    M = 0.0

    #if( (x!=xmax)or(y!=ymax) ):
    #  continue

    #if( (y!=0.0)or(x!=0.0) ):
    #  continue
	  
	  
    I = Iint( x, y )
    print('I(',x,',',y,') =',I)

    J = Jint( x, y )
    print('J(',x,',',y,') =',J)

    K = Kint( x, y )
    print('K(',x,',',y,') =',K)

    M = Mint( x, y )
    print('M(',x,',',y,') =',M)

    Vx = 0.0
    if( x < 0):
      Vx = V - alpha*I
    else:
      Vx = V + alpha*I - beta*J

    Vy = 0.0
    if( x < 0):
      Vy = alpha*K
    else:
      Vy = alpha*K + Q/math.pi*M

    print('v_x(',x,',',y,') =', Vx)
    print('v_y(',x,',',y,') =', Vy)

    vect = []
    vect.append(x);
    vect.append(y);
    vect.append(Vx);
    vect.append(Vy);

    vfield.append(vect)
    
    if( (y == 0.0) ):
      csection.append(vect)
    
    vect = []
    vect.append(x);
    vect.append(y);
    vect.append(Vx-Vx0);
    vect.append(Vy);

    vfield_minus.append(vect)    

    vect = []
    vect.append(x);
    vect.append(y);
    vect.append((Vx-Vx0)*nFactor);
    vect.append(Vy*nFactor);

    vfield_minus_normed.append(vect)
    
    


fileout = open("vfield-analyt-ifs-res.txt", "w") 
  
for vects in vfield:
  print(vects[0],vects[1],vects[2],vects[3])  
  print(vects[0],vects[1],vects[2],vects[3], file=fileout)  
print('', file=fileout)  
  
fileout.close()

 
fileout1 = open("vfield-analyt-ifs-res_minus.txt", "w") 
  
for vects in vfield_minus:
  print(vects[0],vects[1],vects[2],vects[3])  
  print(vects[0],vects[1],vects[2],vects[3], file=fileout1)  
print('', file=fileout1)  
  
fileout1.close()
  

fileout2 = open("vfield-analyt-ifs-res_minus_normed.txt", "w") 
  
for vects in vfield_minus_normed:
  print(vects[0],vects[1],vects[2],vects[3])  
  print(vects[0],vects[1],vects[2],vects[3], file=fileout2)  
print('', file=fileout2)  
  
fileout2.close()


fileout3 = open("vfield-analyt-ifs-res-csection.txt", "w") 
  
for vects in csection:
  print(vects[0],vects[1],vects[2],vects[3])  
  print(vects[0],vects[1],vects[2],vects[3], file=fileout3)  
print('', file=fileout3)  
  
fileout3.close()

	






