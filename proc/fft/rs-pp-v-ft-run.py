from numpy import loadtxt
import numpy as np
import sys

dt = 0.5


def find_vect(x,y,lines):

  xc = lines[0][0]
  yc = lines[0][1]
  dist = (xc - x)*(xc - x) + (yc - y)*(yc - y)
  vx = lines[0][2]
  vy = lines[0][3]

  for line in lines:
    xc = line[0]
    yc = line[1]
    distc = (xc - x)*(xc - x) + (yc - y)*(yc - y)

    if (distc < dist):
      vx = line[2]
      vy = line[3]
      dist = distc


  return (vx, vy)


filename = sys.argv[1]

print('filename =',filename)



lines = loadtxt(filename, comments="#", delimiter=" ", unpack=False)


NN = 0
data=[]


vxlist=[]
vylist=[]

for line in lines:
  #print(line)
  print(line[0],line[1],line[2])
  vxlist.append([line[0],line[1],line[2]])
  vylist.append([line[0],line[1],line[3]])
  NN += 1

N = int(np.sqrt(NN))

print(N)

xs=[]
ys=[]
vx=[]
vy=[]


#for line in lines:
#  #print(line)
#  #print(line[0],line[1],line[2])
#  xs.append(line[0])
#  ys.append(line[1])
#  vx.append(line[2])
#  vy.append(line[3])


fileout = open("vx.txt", "w")
for points in vxlist:
  print(points[0], points[1], points[2], file=fileout)
fileout.close()

fileout = open("vy.txt", "w")
for points in vylist:
  print(points[0], points[1], points[2], file=fileout)
fileout.close()


def kl_range(start, end, step):
  while start <= end:
    yield start
    start += step

dxdy = (vxlist[1][1] - vxlist[0][1])*(vxlist[N][0] - vxlist[0][0])
print('dxdy = ', dxdy)

ks = []
ls = []

k_start = -30
k_end = 30
k_step = 1

l_start = k_start
l_end = k_end
l_step = k_step

Nk = 0
Nl = 0


print("Calculating u_x:")

def ft_vx(k,l):
  fint_re = 0.0
  fint_im = 0.0
  for points in vxlist:
    fint_re = fint_re + points[2]*(  np.cos(k*points[0])*np.cos(l*points[1]) - np.sin(k*points[0])*np.sin(l*points[1]) )*dxdy
    fint_im = fint_im + points[2]*( -np.sin(k*points[0])*np.cos(l*points[1]) - np.cos(k*points[0])*np.sin(l*points[1]) )*dxdy
  return fint_re, fint_im

ux = []
ux_list = []

for k in kl_range(k_start, k_end, k_step):
  Nk = Nk + 1
  for l in kl_range(l_start, l_end, l_step):
    Nl = Nl + 1
    ks.append(k)
    ls.append(l)
    (func_re,func_im) = ft_vx(k,l)
    ux.append([func_re,func_im])
    ux_list.append([k,l,func_re,func_im])
    print(k,l,func_re,func_im)

fileout = open("ux.txt", "w")
for points in ux_list:
  print(points[0], points[1], points[2], points[3], file=fileout)
fileout.close()


print("Calculating u_y:")

def ft_vy(k,l):
  fint_re = 0.0
  fint_im = 0.0
  for points in vylist:
    fint_re = fint_re + points[2]*(  np.cos(k*points[0])*np.cos(l*points[1]) - np.sin(k*points[0])*np.sin(l*points[1]) )*dxdy
    fint_im = fint_im + points[2]*( -np.sin(k*points[0])*np.cos(l*points[1]) - np.cos(k*points[0])*np.sin(l*points[1]) )*dxdy
  return fint_re, fint_im

uy = []
uy_list = []

for k in kl_range(k_start, k_end, k_step):
  Nk = Nk + 1
  for l in kl_range(l_start, l_end, l_step):
    Nl = Nl + 1
    ks.append(k)
    ls.append(l)
    (func_re,func_im) = ft_vy(k,l)
    uy.append([func_re,func_im])
    uy_list.append([k,l,func_re,func_im])
    print(k,l,func_re,func_im)

fileout = open("uy.txt", "w")
for points in uy_list:
  print(points[0], points[1], points[2], points[3], file=fileout)
fileout.close()


exit(0)


