from numpy import loadtxt
import numpy as np
import sys

dt = 0.5


filename = sys.argv[1]
funcname = filename[:-4]


print('filename =',filename)
print('funcname =',funcname)


lines = loadtxt(filename, comments="#", delimiter=" ", unpack=False)


NN = 0
data=[]


glist=[]

for line in lines:
  #print(line)
  print(line[0],line[1],line[2])
  glist.append([line[0],line[1],line[2],line[3]])
  NN += 1

N = int(np.sqrt(NN))

print(N)

xs=[]
ys=[]
vx=[]
vy=[]


#for line in lines:
#  #print(line)
#  #print(line[0],line[1],line[2])
#  xs.append(line[0])
#  ys.append(line[1])
#  vx.append(line[2])
#  vy.append(line[3])


fileout = open("b"+filename, "w")
for points in glist:
  print(points[0], points[1], points[2], points[3], file=fileout)
fileout.close()



def xy_range(start, end, step):
  while start <= end:
    yield start
    start += step

dkdl = (glist[1][1] - glist[0][1])*(glist[N][0] - glist[0][0])
print('dkdk = ', dkdl)

xs = []
ys = []

x_start = 60
x_end = 80
x_step = 1

y_start = -10
y_end = 10
y_step = x_step

Nk = 0
Nl = 0


print("Calculating f = ghat:")

def bft_g(x,y):
  fint_re = 0.0
  fint_im = 0.0
  for points in glist:
    A = np.cos(x*points[0])*np.cos(y*points[1]) - np.sin(x*points[0])*np.sin(y*points[1])
    B = np.sin(x*points[0])*np.cos(y*points[1]) + np.cos(x*points[0])*np.sin(y*points[1])
    fint_re = fint_re + 1.0/np.sqrt(2.0*np.pi)* ( points[2]*A - points[3]*B )*dkdl
    fint_im = fint_im + 1.0/np.sqrt(2.0*np.pi)* ( points[3]*A + points[2]*B )*dkdl
  return fint_re, fint_im

f = []
f_list = []

for x in xy_range(x_start, x_end, x_step):
  Nk = Nk + 1
  for y in xy_range(y_start, y_end, y_step):
    Nl = Nl + 1
    xs.append(x)
    ys.append(y)
    (func_re,func_im) = bft_g(x,y)
    f.append([func_re,func_im])
    f_list.append([x,y,func_re,func_im])
    print(x,y,func_re,func_im)

fileout = open(funcname+"hat.txt", "w")
for points in f_list:
  print(points[0], points[1], points[2], points[3], file=fileout)
fileout.close()


exit(0)


print("Calculating u_y:")

def ft_vy(k,l):
  fint_re = 0.0
  fint_im = 0.0
  for points in vylist:
    fint_re = fint_re + points[2]*(  np.cos(k*points[0])*np.cos(l*points[1]) - np.sin(k*points[0])*np.sin(l*points[1]) )*dxdy
    fint_im = fint_im + points[2]*( -np.sin(k*points[0])*np.cos(l*points[1]) - np.cos(k*points[0])*np.sin(l*points[1]) )*dxdy
  return fint_re, fint_im

uy = []
uy_list = []

for k in kl_range(k_start, k_end, k_step):
  Nk = Nk + 1
  for l in kl_range(l_start, l_end, l_step):
    Nl = Nl + 1
    ks.append(k)
    ls.append(l)
    (func_re,func_im) = ft_vy(k,l)
    uy.append([func_re,func_im])
    uy_list.append([k,l,func_re,func_im])
    print(k,l,func_re,func_im)

fileout = open("uy.txt", "w")
for points in uy_list:
  print(points[0], points[1], points[2], points[3], file=fileout)
fileout.close()
