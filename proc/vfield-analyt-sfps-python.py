from numpy import loadtxt
import sys
import math
import numpy


Nx = 261
Ny = 261

Rx = 160.0
Ry = 160.0

N = 1.0
hf = 4.0
g = 9.81

E = 1.0
R = 1.0

V = math.sqrt(g*R/E)
Q = math.pi*V*R*R

E = g*R/V/V
R = math.sqrt(Q/math.pi/V)


intMax = 10.0
h1d = 0.01
h2d = 0.1

nFactor = 60.0
Vx0 = V

xmin = -Rx/2.0
xmax = Rx/2.0
ymin = -Rx/2.0
ymax = Rx/2.0

stepx = (xmax - xmin)/(Nx - 1)
stepy = (ymax - ymin)/(Ny - 1)

alpha = g*Q/(math.pi*math.pi*V*V)
beta = g*Q/math.pi/V/V
nu = g/V/V

verbose = False


print('Rx =',Rx)
print('Ry =',Ry)
print('V =',V)
print('hf =',hf)
print('Q =',Q)
print('g =',g)
print('N =',N)
print('E =',E)
print('R =',R)
print('alpha =',alpha)
print('beta =',beta)
print('nu =',nu)
print('Nx =',Nx)
print('Ny =',Ny)
print('intMax =',intMax)
print('h1d =',h1d)
print('h2d =',h2d)
print('nFactor =',nFactor)
print('Vx0 =',Vx0)


R_obj = math.sqrt(Q/math.pi/V)
print('Radius of the object =',R_obj)




vfield = []
vfield_minus = []
vfield_minus_normed = []
csection = []

for i in range(Nx):
  for j in range(Ny):
    x = xmin + i*stepx
    y = ymin + j*stepy
    z = hf

    #if( (x!=xmax)or(y!=ymax) ):
    #  continue

    #if( (y!=0.0)or(x!=0.0) ):
    #  continue
	  
    #Vx = 0.0
    #if( x < 0):
    #  Vx = V - alpha*I
    #else:
    #  Vx = V + alpha*I - beta*J


    r = math.sqrt(x*x+y*y+z*z)
    theta = math.acos(x/r)
    phi = math.acos(y/math.sqrt(y*y+z*z))
    
    Vmult = numpy.heaviside(x,0.5)*N*Q/2.0/math.pi/V/r*math.sqrt(math.sin(theta)*math.sin(theta)+math.cos(theta)*math.cos(theta)*math.cos(phi)*math.cos(phi))/math.sin(theta)*math.cos(N/V*r*abs(math.sin(phi)))
    
    Vx =   - Vmult*math.tan(theta)*math.sin(theta)/abs(math.tan(theta))
    Vy =     Vmult*math.sin(theta)*math.cos(phi)/abs(math.tan(theta))
    Vz =     Vmult*math.sin(theta)*math.sin(phi)/abs(math.tan(theta))

    if verbose:
      print('v_x(',x,',',y,') =', Vx)
      print('v_y(',x,',',y,') =', Vy)

    vect = []
    vect.append(x);
    vect.append(y);
    vect.append(Vx);
    vect.append(Vy);
    vect.append(Vz);

    vfield.append(vect)
    
    if( (y == 0.0) ):
      csection.append(vect)
    
    vect = []
    vect.append(x);
    vect.append(y);
    vect.append(Vx-Vx0);
    vect.append(Vy);
    vect.append(Vz);

    vfield_minus.append(vect)    

    vect = []
    vect.append(x);
    vect.append(y);
    vect.append((Vx-Vx0)*nFactor);
    vect.append(Vy*nFactor);
    vect.append(Vz*nFactor);

    vfield_minus_normed.append(vect)
    
    


fileout = open("vfield-analyt-sfps-res.txt", "w") 
  
for vects in vfield:
  if verbose:
    print(vects[0],vects[1],vects[2],vects[3],vects[4])  
  print(vects[0],vects[1],vects[2],vects[3],vects[4], file=fileout)  
print('', file=fileout)  
  
fileout.close()

 
fileout1 = open("vfield-analyt-sfps-res_minus.txt", "w") 
  
for vects in vfield_minus:
  if verbose:
    print(vects[0],vects[1],vects[2],vects[3],vects[4])  
  print(vects[0],vects[1],vects[2],vects[3],vects[4], file=fileout1)  
print('', file=fileout1)  
  
fileout1.close()
  

fileout2 = open("vfield-analyt-sfps-res_minus_normed.txt", "w") 
  
for vects in vfield_minus_normed:
  if verbose:
    print(vects[0],vects[1],vects[2],vects[3],vects[4])  
  print(vects[0],vects[1],vects[2],vects[3],vects[4], file=fileout2)  
print('', file=fileout2)  
  
fileout2.close()


fileout3 = open("vfield-analyt-sfps-res-csection.txt", "w") 
  
for vects in csection:
  if verbose:
    print(vects[0],vects[1],vects[2],vects[3],vects[4])  
  print(vects[0],vects[1],vects[2],vects[3],vects[4], file=fileout3)  
print('', file=fileout3)  
  
fileout3.close()

	






