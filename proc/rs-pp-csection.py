from numpy import loadtxt
import numpy
import sys

dt = 0.5


def find_vect(x,y,lines):

  xc = lines[0][0]
  yc = lines[0][1]
  dist = (xc - x)*(xc - x) + (yc - y)*(yc - y)  
  vx = lines[0][2]
  vy = lines[0][3]
  
  for line in lines:
    xc = line[0]
    yc = line[1]
    distc = (xc - x)*(xc - x) + (yc - y)*(yc - y)
    
    if (distc < dist):
      vx = line[2]
      vy = line[3]
      dist = distc
    
  
  return (vx, vy)


filename = sys.argv[1]

print('filename =',filename)



lines = loadtxt(filename, comments="#", delimiter=" ", unpack=False)


NN = 0
data=[]

for line in lines:
  #print(line)
  #print(line[0],line[1],line[2])
  #append.(  )
  NN += 1

N = int(numpy.sqrt(NN))

print(N)

fileoutN = open(filename[:-4]+"_N.txt", "w") 
print(N, file=fileoutN)  
fileoutN.close()

curve=[]

if N % 2 == 0:
  pass # Even 
else:
  s = (N - 1)/2 + 1
  
  for j in range(0, N):
    for i in range(0, N):
      if i == s:
        curve.append( (lines[i+N*j][0], lines[i+N*j][2]) )
  
  pass # Odd

print(curve)


fileout = open(filename[:-4]+"_csection.txt", "w") 

for points in curve:
  print(points[0],points[1], file=fileout)  
  
fileout.close()


exit(0)


## finding maximum x

xmax = lines[0][0]

for line in lines:
  if line[0] > xmax:
    xmax = line[0]
  
print(xmax)


## finding minimum x

xmin = lines[0][0]

for line in lines:
  if line[0] < xmin:
    xmin = line[0]
  
print(xmin)


## finding all y coordinates

ys = []

for line in lines:
  if line[0] == xmax:
    ys.append(line[1])

print(ys)


fileout = open(filename[:-4]+"_curves.txt", "w") 

curvenum = 0

for y_start in ys:

  curve = []
  curvenum = curvenum + 1
  
  x = xmax
  y = y_start
  
  curve.append( (x,y) )

  while ((x <= xmax) and (x >= xmin) and (y <= ys[-1]) and (y >= ys[0])):
    
    (vx,vy) = find_vect(x,y,lines)
  
    x = x + vx*dt
    y = y + vy*dt
        
    curve.append( (x,y) )
  
  for (xc, yc) in curve:  
    print(xc, yc, file=fileout)  
  print('', file=fileout)  
  
fileout.close()



#print(lines)


print('Number of curves:',curvenum)

fileoutN = open(filename[:-4]+"_N.txt", "w") 
print(curvenum, file=fileoutN)  
fileoutN.close()