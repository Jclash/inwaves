from numpy import loadtxt
import sys
import math
import numpy


Nx = 61
Ny = 61

Rx = 60.0
Ry = 60.0


E = 1.0
hf = 4.0
R = 1.0
g = 9.81

V = math.sqrt(g*R/E)
Q = math.pi*V*R*R

intMax = 10.0
h1d = 0.01
h2d = 0.1

nFactor = 60.0
Vx0 = V

xmin = -Rx/2.0
xmax = Rx/2.0
ymin = -Rx/2.0
ymax = Rx/2.0

stepx = (xmax - xmin)/(Nx - 1)
stepy = (ymax - ymin)/(Ny - 1)

alpha = g*Q/(math.pi*math.pi*V*V)
beta = g*Q/math.pi/V/V
nu = g/V/V


print('Rx =',Rx)
print('Ry =',Ry)
print('V =',V)
print('hf =',hf)
print('Q =',Q)
print('g =',g)
print('E =',E)
print('R =',R)
print('alpha =',alpha)
print('beta =',beta)
print('nu =',nu)
print('Nx =',Nx)
print('Ny =',Ny)
print('intMax =',intMax)
print('h1d =',h1d)
print('h2d =',h2d)
print('nFactor =',nFactor)
print('Vx0 =',Vx0)


R_obj = math.sqrt(Q/math.pi/V)
print('Radius of the object =',R_obj)




vfield = []
vfield_minus = []
vfield_minus_normed = []
csection = []

for i in range(Nx):
  for j in range(Ny):
    x = xmin + i*stepx
    y = ymin + j*stepy
    z = hf

    #if( (x!=xmax)or(y!=ymax) ):
    #  continue

    #if( (y!=0.0)or(x!=0.0) ):
    #  continue
	  
    #Vx = 0.0
    #if( x < 0):
    #  Vx = V - alpha*I
    #else:
    #  Vx = V + alpha*I - beta*J


    R = math.sqrt(x*x+y*y+z*z)
    theta = math.acos(x/R)
    epsilon = math.acos(y/math.sqrt(y*y+z*z))
    
    Vx = V + Q/(4.0*math.pi*R*R)*math.cos(theta)
    Vy = Q/(4.0*math.pi*R*R)*math.sin(theta)*math.cos(epsilon)
    Vz = Q/(4.0*math.pi*R*R)*math.sin(theta)*math.sin(epsilon)

    print('v_x(',x,',',y,') =', Vx)
    print('v_y(',x,',',y,') =', Vy)

    vect = []
    vect.append(x);
    vect.append(y);
    vect.append(Vx);
    vect.append(Vy);

    vfield.append(vect)
    
    if( (y == 0.0) ):
      csection.append(vect)
    
    vect = []
    vect.append(x);
    vect.append(y);
    vect.append(Vx-Vx0);
    vect.append(Vy);

    vfield_minus.append(vect)    

    vect = []
    vect.append(x);
    vect.append(y);
    vect.append((Vx-Vx0)*nFactor);
    vect.append(Vy*nFactor);

    vfield_minus_normed.append(vect)
    
    


fileout = open("vfield-analyt-ifps-res.txt", "w") 
  
for vects in vfield:
  print(vects[0],vects[1],vects[2],vects[3])  
  print(vects[0],vects[1],vects[2],vects[3], file=fileout)  
print('', file=fileout)  
  
fileout.close()

 
fileout1 = open("vfield-analyt-ifps-res_minus.txt", "w") 
  
for vects in vfield_minus:
  print(vects[0],vects[1],vects[2],vects[3])  
  print(vects[0],vects[1],vects[2],vects[3], file=fileout1)  
print('', file=fileout1)  
  
fileout1.close()
  

fileout2 = open("vfield-analyt-ifps-res_minus_normed.txt", "w") 
  
for vects in vfield_minus_normed:
  print(vects[0],vects[1],vects[2],vects[3])  
  print(vects[0],vects[1],vects[2],vects[3], file=fileout2)  
print('', file=fileout2)  
  
fileout2.close()


fileout3 = open("vfield-analyt-ifps-res-csection.txt", "w") 
  
for vects in csection:
  print(vects[0],vects[1],vects[2],vects[3])  
  print(vects[0],vects[1],vects[2],vects[3], file=fileout3)  
print('', file=fileout3)  
  
fileout3.close()

	






