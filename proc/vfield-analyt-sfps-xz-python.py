from numpy import loadtxt
import sys
import math
import numpy


Nx = 261
Ny = 261
Nz = 261

Rx = 200.0
Ry = 200.0
Rz = 200.0

y_section = 0.1

N = 1.0
hf = 4.0
g = 9.81

E = 1.0
R = 1.0

V = 1.0
angle = 30.0
t = 86.0

alpha = math.pi/2 - angle/180.0*math.pi

vx = V*math.cos(angle/180.0*math.pi)
vy = 0.0
vz = V*math.sin(angle/180.0*math.pi)

#vz = 0.0

#vx = math.sqrt(3)/2.0
#vy = 0.0
#vz = 0.5
#V = math.sqrt(vx*vx+vy*vy+vz*vz)
#alpha = math.pi/2 - math.asin(vz/V)  #/2/math.pi*360



#math.sqrt(g*R/E)
Q = math.pi*V*R*R

Q = 1.0

E = g*R/V/V
R = math.sqrt(Q/math.pi/V)


intMax = 10.0
h1d = 0.01
h2d = 0.1

nFactor = 60.0
Vx0 = V

xmin = -Rx/2.0
xmax = Rx/2.0
ymin = -Ry/2.0
ymax = Ry/2.0
zmin = -Rz/2.0
zmax = Rz/2.0

stepx = (xmax - xmin)/(Nx - 1)
stepy = (ymax - ymin)/(Ny - 1)
stepz = (zmax - zmin)/(Nz - 1)

alpha_f = g*Q/(math.pi*math.pi*V*V)
beta_f = g*Q/math.pi/V/V
nu = g/V/V

verbose = False


r = 0.0
theta = 0.0
phi = 0.0
cotThe = 0.0
cosOmega = 0.0
sinThe = 0.0
Vmult = 0.0
tanEta = 0.0

xo1_start = -50.0
yo1_start = 0.0
zo1_start = 0.0

print('Rx =',Rx)
print('Ry =',Ry)
print('V =',V)
print('hf =',hf)
print('Q =',Q)
print('g =',g)
print('N =',N)
print('E =',E)
print('R =',R)
print('alpha_f =',alpha_f)
print('beta_f =',beta_f)
print('nu =',nu)
print('Nx =',Nx)
print('Ny =',Ny)
print('intMax =',intMax)
print('h1d =',h1d)
print('h2d =',h2d)
print('nFactor =',nFactor)
print('Vx0 =',Vx0)
print('alpha =',alpha)
print('vx =',vx)
print('vy =',vy)
print('vz =',vz)


R_obj = math.sqrt(Q/math.pi/V)
print('Radius of the object =',R_obj)

def cubic(a,b,c,d):
  n = -b**3/27/a**3 + b*c/6/a**2 - d/2/a
  s = (n**2 + (c/3/a - b**2/9/a**2)**3)**0.5
  r0 = (n-s)**(1/3)+(n+s)**(1/3) - b/3/a
  r1 = (n+s)**(1/3)+(n+s)**(1/3) - b/3/a
  r2 = (n-s)**(1/3)+(n-s)**(1/3) - b/3/a
  return r0, r1, r2
  

def calc_cg_A(tanEta):

  good = 1
  
  cosEta = 1/math.sqrt(1+tanEta*tanEta)
  if vx/vz*z - x < 0.0:
    cosEta = -1/math.sqrt(1+tanEta*tanEta)
        
        #print(tanEta)
      
  Rx = r*(math.sin(theta)*math.cos(phi)*math.cos(alpha)*math.cos(alpha)+math.sin(alpha)*sinThe*tanEta-math.sin(alpha)*math.cos(alpha)*math.cos(theta))
  Ry = r*math.sin(theta)*math.sin(phi)
  Rz = r*(math.cos(theta)*math.sin(alpha)*math.sin(alpha)+math.cos(alpha)*sinThe*tanEta-math.sin(alpha)*math.cos(alpha)*math.sin(theta)*math.cos(phi))
      
  R = math.sqrt(Rx*Rx+Ry*Ry+Rz*Rz)
    
        #cg_x = (vx*Rx/R+vy*Ry/R+vz*(Rz/R-R/Rz))*Rx/R
        #cg_y = (vx*Rx/R+vy*Ry/R+vz*(Rz/R-R/Rz))*Ry/R
        #cg_z = (vx*Rx/R+vy*Ry/R+vz*(Rz/R-R/Rz))*Rz/R
        
  cg_x = 1.0
  cg_y = 0.0
  cg_z = 0.0

  if (math.cos(alpha)*tanEta+cosOmega != 0):
    cg_x = (V*cosEta*cosEta/r/sinThe)*(cosOmega*tanEta-math.cos(alpha))/(math.cos(alpha)*tanEta+cosOmega)*Rx
    cg_y = (V*cosEta*cosEta/r/sinThe)*(cosOmega*tanEta-math.cos(alpha))/(math.cos(alpha)*tanEta+cosOmega)*Ry
    cg_z = (V*cosEta*cosEta/r/sinThe)*(cosOmega*tanEta-math.cos(alpha))/(math.cos(alpha)*tanEta+cosOmega)*Rz    
  else:
    good = 0   
    

      
  cg = math.sqrt(cg_x*cg_x+cg_y*cg_y+cg_z*cg_z)
    
  #print(Rx, Ry, Rz, cg)
      
  vm_x = vy*Rz - vz*Ry
  vm_y = vz*Rx - vx*Rz
  vm_z = vx*Ry - vy*Rx

  A = 1.0  
  
  if (Rz != 0):
    A = -(vm_x*vm_x+vm_y*vm_y+vm_z*vm_z)/cg/cg/R/R + 2*(vz/cg*R/Rz)
  else:
    good = 0  
    
      #print(A)
  Vmult = 0.0
      
  if A != 0:
    Vmult = N*Q*math.sqrt(Rx*Rx+Ry*Ry)/2/math.pi/cg/R/R/R/math.sqrt(abs(A))*math.cos(N*abs(Rz)/cg+math.pi*numpy.heaviside(A,1.0)/2)
  else:
    good = 0  
  
  Vx = Vmult*Rx
  Vy = Vmult*Ry
  Vz = Vmult*Rz
  
  tau_s = t - r*sinThe/V/cosEta/cosEta*(math.cos(alpha)*tanEta+cosOmega)/(cosOmega*tanEta-math.cos(alpha))
  if tau_s < 0:
    good = 0
    
  #if x > xo1 and z > zo1:
  #  good = 0    
  
  return Vx, Vy, Vz, good


vfield = []
vfield_minus = []
vfield_minus_normed = []
csection = []

for i in range(Nx):
  for k in range(Nz):
    x = xmin + i*stepx
    y = y_section
    z = zmin + k*stepz

    #if( (x!=xmax)or(y!=ymax) ):
    #  continue

    #if( (y!=0.0)or(x!=0.0) ):
    #  continue
	  
    #Vx = 0.0
    #if( x < 0):
    #  Vx = V - alpha*I
    #else:
    #  Vx = V + alpha*I - beta*J
    
   
    Vx = 0.0
    Vy = 0.0
    Vz = 0.0
    
    if vz == 0.0:
    
      x1 = x

      x = -x + Rx/4.0

      r = math.sqrt(x*x+y*y+z*z)
      theta = math.acos(x/r)
      phi = math.acos(y/math.sqrt(y*y+z*z))    
    
      Vmult = numpy.heaviside(x,0.5)*N*Q/2.0/math.pi/V/r*math.sqrt(math.sin(theta)*math.sin(theta)+math.cos(theta)*math.cos(theta)*math.cos(phi)*math.cos(phi))/math.sin(theta)*math.cos(N/V*r*abs(math.sin(phi)))
    
      Vx =   - Vmult*math.tan(theta)*math.sin(theta)/abs(math.tan(theta))
      Vy =     Vmult*math.sin(theta)*math.cos(phi)/abs(math.tan(theta))
      Vz =     Vmult*math.sin(theta)*math.sin(phi)/abs(math.tan(theta))
      
      x = x1;
    
    else:

      xo1 = xo1_start + vx*t 
      yo1 = yo1_start + vy*t
      zo1 = zo1_start + vz*t
    
      r = math.sqrt((x-xo1)*(x-xo1)+(y-yo1)*(y-yo1)+(z-zo1)*(z-zo1))
      theta = math.acos((z-zo1)/r)
      phi = math.acos((x-xo1)/math.sqrt((y-yo1)*(y-yo1)+(x-xo1)*(x-xo1)))
      
      if y < 0:
        phi = -1*phi
    
      cotThe = (math.sin(theta)*math.cos(phi)*math.sin(alpha)+math.cos(theta)*math.cos(alpha))/math.sqrt(math.sin(theta)*math.sin(theta)*math.sin(phi)*math.sin(phi)+(math.cos(theta)*math.sin(alpha)-math.sin(theta)*math.cos(phi)*math.cos(alpha))*(math.cos(theta)*math.sin(alpha)-math.sin(theta)*math.cos(phi)*math.cos(alpha)))
      
      cosOmega = (math.cos(theta)*math.sin(alpha)-math.sin(theta)*math.cos(phi)*math.cos(alpha))*math.sin(alpha)/math.sqrt(math.sin(theta)*math.sin(theta)*math.sin(phi)*math.sin(phi)+(math.cos(theta)*math.sin(alpha)-math.sin(theta)*math.cos(phi)*math.cos(alpha))*(math.cos(theta)*math.sin(alpha)-math.sin(theta)*math.cos(phi)*math.cos(alpha)))
    
      sinThe = 1/math.sqrt(1+cotThe*cotThe)  # +-
      #if vx/vz*z - x < 0.0:
      #  sinThe = -1/math.sqrt(1+cotThe*cotThe)  
      
      # from a cubic
      a3 = 1.0
      a2 = 0.0
      a1 = (cosOmega/math.cos(alpha)*cotThe+2)
      a0 = cosOmega/math.cos(alpha) - cotThe
      
      #r1, r2, r3 = cubic(a3,a2,a1,a0)
      
      r1, r2, r3 = numpy.roots([a3,a2,a1,a0])
      
      #print(cubic(a3,a2,a1,a0))

      Vmult = 0.0
      
      tanEta = 1.0
      
      Vx = 0.0
      Vy = 0.0
      Vz = 0.0
      
      Vx1 = 0.0
      Vy1 = 0.0
      Vz1 = 0.0
      
      Vx2 = 0.0
      Vy2 = 0.0
      Vz2 = 0.0      
      
      Vx3 = 0.0
      Vy3 = 0.0
      Vz3 = 0.0      
      
      good1 = 1
      good2 = 1
      good3 = 1
      
      if numpy.isreal(r1) or numpy.isreal(r2) or numpy.isreal(r3):
      
        if numpy.isreal(r1):
          tanEta = float(r1.real)
          Vx1, Vy1, Vz1, good1 = calc_cg_A(tanEta)
        if numpy.isreal(r2):
          tanEta = float(r2.real)
          Vx2, Vy2, Vz2, good2 = calc_cg_A(tanEta)
        if numpy.isreal(r3):
          tanEta = float(r3.real)  
          Vx3, Vy3, Vz3, good3 = calc_cg_A(tanEta)

      Vx = Vx1*good1 + Vx2*good2 + Vx3*good3
      Vy = Vy1*good1 + Vy2*good2 + Vy3*good3      
      Vz = Vz1*good1 + Vz2*good2 + Vz3*good3
    

    if verbose:
      print('v_x(',x,',',y,',',z,') =', Vx)
      print('v_y(',x,',',y,',',z,') =', Vy)
      print('v_z(',x,',',y,',',z,') =', Vz)

    vect = []
    vect.append(x);
    vect.append(z);
    vect.append(Vx);
    vect.append(Vy);
    vect.append(Vz);

    vfield.append(vect)
    
    if( (z == 0.0) ):
      csection.append(vect)
    
    vect = []
    vect.append(x);
    vect.append(z);
    vect.append(Vx-Vx0);
    vect.append(Vy);
    vect.append(Vz);

    vfield_minus.append(vect)    

    vect = []
    vect.append(x);
    vect.append(z);
    vect.append((Vx-Vx0)*nFactor);
    vect.append(Vy*nFactor);
    vect.append(Vz*nFactor);

    vfield_minus_normed.append(vect)
    
    


fileout = open("vfield-analyt-sfps-xz-res.txt", "w") 
  
for vects in vfield:
  if verbose:
    print(vects[0],vects[1],vects[2],vects[3],vects[4])  
  print(vects[0],vects[1],vects[2],vects[3],vects[4], file=fileout)  
print('', file=fileout)  
  
fileout.close()

 
fileout1 = open("vfield-analyt-sfps-xz-res_minus.txt", "w") 
  
for vects in vfield_minus:
  if verbose:
    print(vects[0],vects[1],vects[2],vects[3],vects[4])  
  print(vects[0],vects[1],vects[2],vects[3],vects[4], file=fileout1)  
print('', file=fileout1)  
  
fileout1.close()
  

fileout2 = open("vfield-analyt-sfps-xz-res_minus_normed.txt", "w") 
  
for vects in vfield_minus_normed:
  if verbose:
    print(vects[0],vects[1],vects[2],vects[3],vects[4])  
  print(vects[0],vects[1],vects[2],vects[3],vects[4], file=fileout2)  
print('', file=fileout2)  
  
fileout2.close()


fileout3 = open("vfield-analyt-sfps-xz-res-csection.txt", "w") 
  
for vects in csection:
  if verbose:
    print(vects[0],vects[1],vects[2],vects[3],vects[4])  
  print(vects[0],vects[1],vects[2],vects[3],vects[4], file=fileout3)  
print('', file=fileout3)  
  
fileout3.close()

	






