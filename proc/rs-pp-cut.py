from numpy import loadtxt
import sys
import math

dt = 0.5


xc = 20.0
yc = 0.0
side = 60.0

vx0 = 3.132092

nFactor = 60.0;
skip = 1;

x1 = xc - side/2.0
y1 = yc - side/2.0
x2 = xc + side/2.0
y2 = yc + side/2.0


filename = sys.argv[1]

print('filename =',filename)



lines = loadtxt(filename, comments="#", delimiter=" ", unpack=False)

## finding maximum x

newlines = []

print(len(lines))

N = math.sqrt(len(lines))

print("N = ",N)

curx = 0
cury = 0

for line in lines:

  if( ( curx % skip == 0)or( cury % skip == 0 )):

    if ( (line[0] >= x1) and (line[0] <= x2) and (line[1] >= y1) and (line[1] <= y2) ):
  
      # initial vector field
      line[2] = vx0 - line[2]
      line[0] = xc - line[0]
    
      # uncomment for delta of vector field
      #line[2] = nFactor*(line[2] - vx0)
      #line[3] = nFactor*line[3]
    
      newlines.append(line)    
  
  curx = curx + 1
  if( curx == N):
    curx = 0
    cury = cury + 1
 
#print(newlines)



fileout = open("cut_minus_"+filename, "w")

#print(lines)


for line in newlines:
  print(line[0], line[1], line[2], line[3], file=fileout)

fileout.close()
